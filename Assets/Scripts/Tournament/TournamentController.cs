﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class TournamentController : MonoBehaviour
{

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject canvas;
    public Text buyin;
    public Text first;
    public Text second;
    public Text third;
    public Dropdown speed;
    public Slider slider;
    public ScrollRect chatScrollView;
    public Text chatText;
    public CanvasGroup chatControls;
    public Text loggedInText;
    public Transform gameListContent;
    public GameObject gameListItem;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private const string EXTENSION_ID = "PokerExtension";
    private const string EXTENSION_CLASS = "org.dsaw.poker.PokerExtension";

    private SFSManager sfsManager;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            Application.LoadLevel("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();

        // Populate list of available games
        populateGamesList();

        // Disable chat controls until the lobby Room is joined successfully
        chatControls.interactable = true;
        canvas.GetComponent<CanvasGroup>().interactable = !GlobalInfo.isCoach;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackButtonClick();
        }
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnSendMessageButtonClick()
    {
        InputField msgField = (InputField)chatControls.GetComponentInChildren<InputField>();
        if (msgField.text != "")
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("senderName", GlobalInfo.name);
            // Send public message to Room
            sfsManager.sfs.Send(new Sfs2X.Requests.PublicMessageRequest(msgField.text, obj, sfsManager.sfs.LastJoinedRoom));

            // Reset message field
            msgField.text = "";
            //msgField.ActivateInputField();
            //msgField.Select();
        }
    }

    public void OnSendMessageKeyPress()
    {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            OnSendMessageButtonClick();
    }

    public void OnBackButtonClick()
    {
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "loadLevel");
            obj.PutUtfString("param", "Main Screen");
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }
        Application.LoadLevel("Main Screen");
    }

    public void OnGameItemClick(int roomId)
    {
        // Disable chaat controls
        chatControls.interactable = false;

        // Join the Room
        sfsManager.sfs.Send(new Sfs2X.Requests.JoinRoomRequest(roomId));
    }

    public void OnStartNewGameButtonClick()
    {
        //// Configure Game Room
        //RoomSettings settings = new RoomSettings(GlobalInfo.name + "'s game");
        //settings.GroupId = "Tournament";
        //settings.IsGame = true;
        //settings.Extension = new RoomExtension(EXTENSION_ID, EXTENSION_CLASS);

        //// Request Game Room creation to server
        //sfsManager.sfs.Send(new CreateRoomRequest(settings, true, sfsManager.sfs.LastJoinedRoom));

        //int i = 0;
        //string roomName = GlobalInfo.name + "'s game";
        //while (true)
        //{
        //}

        ISFSObject obj = new SFSObject();
        obj.PutUtfString("roomName", GlobalInfo.name + "'s game");
        obj.PutUtfString("groupId", "Tournament");
        obj.PutBool("isGame", true);
        obj.PutUtfString("extensionId", EXTENSION_ID);
        obj.PutUtfString("extensionClass", EXTENSION_CLASS);
        sfsManager.sfs.Send(new ExtensionRequest("createRoom", obj, null));
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void printSystemMessage(string message)
    {
        chatText.text += "<color=#808080ff>" + message + "</color>\n";

        // Scroll view to bottom
        chatScrollView.verticalNormalizedPosition = 0;
    }

    public void printUserMessage(User user, string message, SFSObject obj)
    {
        chatText.text += "<b>" + (user == sfsManager.sfs.MySelf ? "You" : obj.GetUtfString("senderName")) + ":</b> " + message + "\n";

        // Scroll view to bottom
        chatScrollView.verticalNormalizedPosition = 0;
    }

    public void populateGamesList()
    {
        // For the gamelist we use a scrollable area containing a separate prefab button for each Game Room
        // Buttons are clickable to join the games
        List<Room> rooms = sfsManager.sfs.RoomManager.GetRoomList();

        foreach (Room room in rooms)
        {
            // Show only game rooms
            // Also password protected Rooms are skipped, to make this example simpler
            // (protection would require an interface element to input the password)
            if (room.GroupId != "Tournament" || !room.IsGame || room.IsHidden || room.IsPasswordProtected)
            {
                continue;
            }

            int roomId = room.Id;

            GameObject newListItem = Instantiate(gameListItem) as GameObject;
            GameListItem roomItem = newListItem.GetComponent<GameListItem>();
            roomItem.nameLabel.text = room.Name;
            roomItem.roomId = roomId;

            roomItem.button.onClick.AddListener(() => OnGameItemClick(roomId));

            newListItem.transform.SetParent(gameListContent, false);
        }
    }

    public void clearGamesList()
    {
        foreach (Transform child in gameListContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void onJoinClick()
    {
        GlobalInfo.tableSize = 9;
        GlobalInfo.isFast = (speed.value == 0);
        int sliderVal = (int)slider.value;
        string roomName = "Tournament_" + sliderVal + "_" + speed.value;
        Room room = sfsManager.sfs.GetRoomByName(roomName);
        GlobalInfo.buyin = 500 * sliderVal;
        GlobalInfo.blind = 10 * sliderVal;
        if (room == null)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("roomName", roomName);
            obj.PutUtfString("groupId", "Tournament");
            obj.PutBool("isGame", true);
            obj.PutUtfString("extensionId", EXTENSION_ID);
            obj.PutUtfString("extensionClass", EXTENSION_CLASS);
            obj.PutInt("tableSize", GlobalInfo.tableSize);
            obj.PutBool("isFast", GlobalInfo.isFast);
            obj.PutInt("buyin", 500 * sliderVal);
            obj.PutInt("blind", 10 * sliderVal);
            sfsManager.sfs.Send(new ExtensionRequest("createRoom", obj, null));
        }
        else
        {
            // Join the Room
            sfsManager.sfs.Send(new Sfs2X.Requests.JoinRoomRequest(roomName));
        }
    }

    public void onSliderValChange()
    {
        int sliderVal = (int)slider.value;
        buyin.text = "$" + (500 * sliderVal) + " + $" + (10 * sliderVal);
        first.text = "1st place: $" + (int)(500 * sliderVal * 4.5f);
        second.text = "2nd place: $" + (int)(500 * sliderVal * 2.7f);
        third.text = "3rd place: $" + (int)(500 * sliderVal * 1.8f);
    }
}
