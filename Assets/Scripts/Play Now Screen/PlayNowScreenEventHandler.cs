﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class PlayNowScreenEventHandler : MonoBehaviour
{

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject canvas;
    public GameObject detail;
    public Text caption;
    public Dropdown tableSize;
    public Dropdown speed;
    public Dropdown buyin;
    public Dropdown bigblind;
    public Camera mainCamera;
    public GameObject earth;
    public GameObject zonePanel;
    public GameObject lobbyPanel;
    public ScrollRect chatScrollView;
    public Text title;
    public Text chatText;
    public CanvasGroup chatControls;
    public Text loggedInText;
    public Transform gameListContent;
    public GameObject gameListItem;

    public bool isAvailable = false;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private const string EXTENSION_ID = "PokerExtension";
    private const string EXTENSION_CLASS = "org.dsaw.poker.PokerExtension";

    private SFSManager sfsManager;
    private bool isLobby = false;
    private Vector2 prevPos;
    private bool isEarthMove = false;
    private string location;
    private Dictionary<string, ISFSObject> locationList = new Dictionary<string, ISFSObject>();

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            Application.LoadLevel("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        canvas.GetComponent<CanvasGroup>().interactable = !GlobalInfo.isCoach;

        sfsManager.sfs.Send(new ExtensionRequest("locationInfo", new SFSObject(), null));
    }

    // Update is called once per frame
    void Update()
    {
        //if (!isAvailable)
        //    return;
        if (GlobalInfo.isCoachMode && GlobalInfo.isCoach)
            return;
        if (!isLobby && !GlobalInfo.isCoach)
        {
            //if (Input.GetMouseButtonDown(0))
            //{
            //    RaycastHit hitInfo;
            //    Ray myRay = mainCamera.ScreenPointToRay(Input.mousePosition);
            //    prevPos = Input.mousePosition;
            //    //print(Input.mousePosition);
            //    if (Physics.Raycast(myRay, out hitInfo, Mathf.Infinity))
            //    {
            //        if (hitInfo.collider.tag == "Location")
            //        {
            //            print(hitInfo.collider.transform.gameObject.name);
            //            selectZone(hitInfo.collider.transform.gameObject.name);
            //            isEarthMove = true;
            //        }
            //        else if (hitInfo.collider.tag == "Earth")
            //            isEarthMove = true;
            //    }
            //}
            //if (Input.GetMouseButton(0) && isEarthMove)
            //{
            //    float mx = prevPos.x - Input.mousePosition.x;
            //    float my = prevPos.y - Input.mousePosition.y;

            //    earth.transform.Rotate(new Vector3(-my * 0.5f, mx * 0.5f, 0), Space.World);
            //    prevPos = Input.mousePosition;
            //}
            //if (Input.GetMouseButtonUp(0))
            //    isEarthMove = false;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackButtonClick();
        }
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnSendMessageButtonClick()
    {
        InputField msgField = (InputField)chatControls.GetComponentInChildren<InputField>();
        if (msgField.text != "")
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("senderName", GlobalInfo.name);
            // Send public message to Room
            sfsManager.sfs.Send(new Sfs2X.Requests.PublicMessageRequest(msgField.text, obj, sfsManager.sfs.LastJoinedRoom));

            // Reset message field
            msgField.text = "";
            msgField.ActivateInputField();
            msgField.Select();
        }
    }

    public void OnSendMessageKeyPress()
    {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            OnSendMessageButtonClick();
    }

    public void OnBackButtonClick()
    {
        if(lobbyPanel.active == true)
        {
            if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
            {
                ISFSObject obj = new SFSObject();
                obj.PutUtfString("receiver", GlobalInfo.coachEmail);
                obj.PutUtfString("action", "PlayNowBack");
                sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
            }
            isLobby = false;
            zonePanel.SetActive(true);
            lobbyPanel.SetActive(false);
        }
        else
        {
            if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
            {
                ISFSObject obj = new SFSObject();
                obj.PutUtfString("receiver", GlobalInfo.coachEmail);
                obj.PutUtfString("action", "loadLevel");
                obj.PutUtfString("param", "Main Screen");
                sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
            }
            Application.LoadLevel("Main Screen");
        }
    }

    public void OnGameItemClick(int roomId)
    {
        // Disable chaat controls
        chatControls.interactable = false;

        // Join the Room
        sfsManager.sfs.Send(new Sfs2X.Requests.JoinRoomRequest(roomId));
    }

    public void OnStartNewGameButtonClick()
    {
        //// Configure Game Room
        //RoomSettings settings = new RoomSettings(GlobalInfo.name + "'s game");
        //settings.GroupId = GlobalInfo.zone;
        //settings.IsGame = true;
        //settings.Extension = new RoomExtension(EXTENSION_ID, EXTENSION_CLASS);

        //// Request Game Room creation to server
        //sfsManager.sfs.Send(new CreateRoomRequest(settings, true, sfsManager.sfs.LastJoinedRoom));

        ISFSObject obj = new SFSObject();
        obj.PutUtfString("roomName", GlobalInfo.name + "'s game");
        obj.PutUtfString("groupId", GlobalInfo.zone);
        obj.PutBool("isGame", true);
        obj.PutUtfString("extensionId", EXTENSION_ID);
        obj.PutUtfString("extensionClass", EXTENSION_CLASS);
        sfsManager.sfs.Send(new ExtensionRequest("createRoom", obj, null));
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void printSystemMessage(string message)
    {
        chatText.text += "<color=#808080ff>" + message + "</color>\n";

        // Scroll view to bottom
        chatScrollView.verticalNormalizedPosition = 0;
    }

    public void printUserMessage(User user, string message, SFSObject obj)
    {
        chatText.text += "<b>" + (user == sfsManager.sfs.MySelf ? "You" : obj.GetUtfString("senderName")) + ":</b> " + message + "\n";

        // Scroll view to bottom
        chatScrollView.verticalNormalizedPosition = 0;
    }

    public void populateGamesList()
    {
        // For the gamelist we use a scrollable area containing a separate prefab button for each Game Room
        // Buttons are clickable to join the games
        List<Room> rooms = sfsManager.sfs.RoomManager.GetRoomList();

        foreach (Room room in rooms)
        {
            // Show only game rooms
            // Also password protected Rooms are skipped, to make this example simpler
            // (protection would require an interface element to input the password)
            if (room.GroupId != GlobalInfo.zone || !room.IsGame || room.IsHidden || room.IsPasswordProtected)
            {
                continue;
            }

            int roomId = room.Id;

            GameObject newListItem = Instantiate(gameListItem) as GameObject;
            GameListItem roomItem = newListItem.GetComponent<GameListItem>();
            roomItem.nameLabel.text = room.Name;
            roomItem.roomId = roomId;

            roomItem.button.onClick.AddListener(() => OnGameItemClick(roomId));

            newListItem.transform.SetParent(gameListContent, false);
        }
    }

    public void clearGamesList()
    {
        foreach (Transform child in gameListContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void selectZone(string _zone)
    {
        ISFSObject obj = new SFSObject();
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "selectZone");
            obj.PutUtfString("param", _zone);
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }

        detail.SetActive(true);
        location = _zone;
        caption.text = location;

        obj = locationList[_zone];
        buyin.options[0].text = obj.GetInt("buy_in").ToString();
        bigblind.options[0].text = obj.GetInt("big_blind").ToString();
        buyin.RefreshShownValue();
        bigblind.RefreshShownValue();
        //buyin.text = obj.GetInt("buy_in").ToString();
        //bigblind.text = obj.GetInt("big_blind").ToString();
        GlobalInfo.location = location;
        GlobalInfo.buyin = obj.GetInt("buy_in");
        GlobalInfo.blind = obj.GetInt("big_blind");

        //isLobby = true;
        //zonePanel.SetActive(false);
        //lobbyPanel.SetActive(true);
        //title.text = zone;

        //// Populate list of available games
        //populateGamesList();

        //// Disable chat controls until the lobby Room is joined successfully
        //chatControls.interactable = true;
    }

    public void getInfo(SFSObject obj)
    {
        isAvailable = true;
        ISFSArray array = obj.GetSFSArray("infoArray");
        foreach(SFSObject item in array)
        {
            locationList.Add(item.GetUtfString("name"), item);
        }
    }

    public void onPlayGame()
    {
        GlobalInfo.tableSize = (tableSize.value == 0) ? 6 : 9;
        GlobalInfo.isFast = (speed.value == 0);
        string roomName = location + "_" + GlobalInfo.tableSize + "_" + speed.value;
        Room room = sfsManager.sfs.GetRoomByName(roomName);
        if (room == null)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("roomName", roomName);
            obj.PutUtfString("groupId", "Game");
            obj.PutBool("isGame", true);
            obj.PutUtfString("extensionId", EXTENSION_ID);
            obj.PutUtfString("extensionClass", EXTENSION_CLASS);
            obj.PutInt("tableSize", GlobalInfo.tableSize);
            obj.PutBool("isFast", GlobalInfo.isFast);
            obj.PutInt("buyin", GlobalInfo.buyin);
            obj.PutInt("blind", GlobalInfo.blind);
            sfsManager.sfs.Send(new ExtensionRequest("createRoom", obj, null));
        }
        else
        {
            // Join the Room
            sfsManager.sfs.Send(new Sfs2X.Requests.JoinRoomRequest(roomName));
        }
    }

}
