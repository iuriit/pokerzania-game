﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class MainScreenEventHandler : MonoBehaviour {

    public GameObject buttonParent;
    public GameObject[] buttons;
    public Sprite[] buttonImages;
    public GameObject panel;
    public GameObject settingsPanel;
    public GameObject avatarPanel;
    public Text onlinePlayerNumber;
    public GameObject createAccountButton;
    public GameObject profileImage;
    public GameObject coachStatus;
    public Image avatarImage;
    public Text chipText;
    public Slider levelSlider;
    public Text levelText;

    private SFSManager sfsManager;
    private Vector3[] orgPos = new Vector3[4];
    private Vector3[] orgScale = new Vector3[4];
    private bool isClick = false;
    private bool isMove = false;
    private int dir = 0;
    private float timer = 0;
    private Vector2 prevPos;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            Application.LoadLevel("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        panel.GetComponent<CanvasGroup>().interactable = !GlobalInfo.isCoach;
        setAvatar(GlobalInfo.avatar);
        for (int i = 0; i < 4; i++)
        {
            orgPos[i] = buttons[i].transform.localPosition;
            orgScale[i] = buttons[i].transform.localScale;
        }
        setButtons();
        sfsManager.sfs.Send(new ExtensionRequest("onlinePlayerNumber", new SFSObject(), null));
        sfsManager.sfs.Send(new ExtensionRequest("cash", new SFSObject(), null));

        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("email", GlobalInfo.email);
        sfsManager.sfs.Send(new ExtensionRequest("getLevel", resObj, null));
    }

    private void setButtons()
    {
        for (int i = 0; i < 4; i++)
        {
            buttons[i].transform.localPosition = orgPos[i];
            buttons[i].transform.localScale = orgScale[i];
            buttons[i].GetComponent<Image>().sprite = buttonImages[(GlobalInfo.buttonState + 4 + i) % 4];
        }
        createAccountButton.SetActive(GlobalInfo.isGuest);
        profileImage.SetActive(!GlobalInfo.isGuest);
    }

    // Update is called once per frame
    void Update()
    {
        coachStatus.SetActive(GlobalInfo.isCoachMode);
        if (GlobalInfo.isCoachMode && GlobalInfo.isCoach)
            return;

        timer += Time.deltaTime;
        if(isMove)
        {
            for(int i = 0; i < 4; i ++)
            {
                buttons[i].transform.localPosition = Vector3.Lerp(orgPos[i], orgPos[(i + dir + 4) % 4], timer * 2);
                buttons[i].transform.localScale = Vector3.Lerp(orgScale[i], orgScale[(i + dir + 4) % 4], timer * 2);
            }
            if(timer >= 0.5f)
            {
                isMove = false;
                setButtons();
            }
            return;
        }

        if (settingsPanel.active)
            return;

        Vector2 pos = Input.mousePosition;
        if (Input.GetMouseButtonDown(0))
        {
            if (pos.y > Screen.height / 4 && pos.y < Screen.height - Screen.height / 4)
            {
                prevPos = Input.mousePosition;
                isClick = true;
            }
        }
        if (Input.GetMouseButtonUp(0) && isClick)
        {
            isClick = false;
            if(Input.mousePosition.x < prevPos.x - 10)
            {
                GlobalInfo.buttonState = (GlobalInfo.buttonState + 3) % 4;
                dir = 1;
                timer = 0;
                isMove = true;
                buttons[2].transform.SetSiblingIndex(0);
                buttons[1].transform.SetSiblingIndex(1);
                buttons[3].transform.SetSiblingIndex(2);
                buttons[0].transform.SetSiblingIndex(3);
            }
            else if (Input.mousePosition.x > prevPos.x + 10)
            {
                GlobalInfo.buttonState = (GlobalInfo.buttonState + 1) % 4;
                dir = -1;
                timer = 0;
                isMove = true;
                buttons[2].transform.SetSiblingIndex(0);
                buttons[3].transform.SetSiblingIndex(1);
                buttons[1].transform.SetSiblingIndex(2);
                buttons[0].transform.SetSiblingIndex(3);
            }
        }
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnLeaderboardButton()
    {
        Application.LoadLevel("Leaderboard");
    }

    public void OnCreateAccountButtonPressed()
    {
        GlobalInfo.isLobby = true;
        Application.LoadLevel("CreateAccount");
    }

    public void OnMidButtonPressed()
    {
        if (isMove)
            return;
        if (Mathf.Abs(Input.mousePosition.x - prevPos.x) > 10)
            return;
        switch(GlobalInfo.buttonState)
        {
            case 0:
                OnPlayButtonPressed();
                break;
            case 1:
                OnTournamentButtonPressed();
                break;
            case 2:
                OnClubButtonPressed();
                break;
            case 3:
                OnCoachButtonPressed();
                break;
        }
    }

    public void OnPlayButtonPressed()
    {
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "loadLevel");
            obj.PutUtfString("param", "Play Now Screen");
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }
        Application.LoadLevel("Play Now Screen");
    }

    public void OnCoachButtonPressed()
    {
        if (isMove)
            return;
        //if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        //{
        //    ISFSObject obj = new SFSObject();
        //    obj.PutUtfString("receiver", GlobalInfo.coachEmail);
        //    obj.PutUtfString("action", "loadLevel");
        //    obj.PutUtfString("param", "Coach List");
        //    sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        //}
        Application.LoadLevel("Coach List");
    }

    public void OnTournamentButtonPressed()
    {
        if (isMove)
            return;
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "loadLevel");
            obj.PutUtfString("param", "Tournament");
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }
        Application.LoadLevel("Tournament");
    }

    public void OnClubButtonPressed()
    {
        if (isMove)
            return;
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "loadLevel");
            obj.PutUtfString("param", "Club");
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }
        Application.LoadLevel("Club");
    }

    public void OnProfileButtonPressed()
    {
        if (isMove)
            return;
        if (GlobalInfo.isGuest)
            return;
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "loadLevel");
            obj.PutUtfString("param", "Profile");
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }
        Application.LoadLevel("Profile");
    }

    public void OnSettingsButtonPressed()
    {
        if (isMove)
            return;
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "loadLevel");
            obj.PutUtfString("param", "Settings");
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }
        //Application.LoadLevel("Settings");
        settingsPanel.SetActive(true);
    }

    public void OnAvatarPressed()
    {
        if (isMove)
            return;
        panel.SetActive(false);
        avatarPanel.SetActive(true);
    }

    public void OnAvatarChoose(int cmd)
    {
        GlobalInfo.avatar = cmd;

        ISFSObject obj = new SFSObject();
        obj.PutInt("avatar", cmd);
        sfsManager.sfs.Send(new ExtensionRequest("setAvatar", obj, null));

        panel.SetActive(true);
        avatarPanel.SetActive(false);
        setAvatar(cmd);
    }

    // Public Helpers

    public void setAvatar(int cmd)
    {
        Avatar avt = (Avatar)GameObject.FindObjectOfType(typeof(Avatar));
        avatarImage.sprite = avt.avatar[cmd];
    }

    public void setOnlinePlayerNumber(SFSObject obj)
    {
        onlinePlayerNumber.text = string.Format("{0:n0}", obj.GetInt("number")) + " player(s) online";
    }

    public void updateCash()
    {
        chipText.text = GlobalInfo.cash.ToString();
    }

    public void getLevel(SFSObject obj)
    {
        levelSlider.value = Mathf.Ceil((float)obj.GetDouble("hour"));
        levelText.text = "level " + levelSlider.value;
    }

}
