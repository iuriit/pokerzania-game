﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerItem : MonoBehaviour {
    public Image avatar;
    public Text nameText;
    public Text RankingText;
    public Text chipText;
    public GameObject badge;
    public Slider levelSlider;
}
