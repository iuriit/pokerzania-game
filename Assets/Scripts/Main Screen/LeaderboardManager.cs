﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class LeaderboardManager : MonoBehaviour
{

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject canvas;
    public GameObject listPanel;

    public Transform listContent;
    public GameObject playerItem;

    public GameObject[] searchButtons;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;
    private Avatar avatarController;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            Application.LoadLevel("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        avatarController = (Avatar)GameObject.FindObjectOfType(typeof(Avatar));
        canvas.GetComponent<CanvasGroup>().interactable = !GlobalInfo.isCoach;

        OnSelectSearchField(0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackButtonClick();
        }
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnBackButtonClick()
    {
        Application.LoadLevel("Main Screen");
    }

    public void OnSelectSearchField(int cmd)
    {
        for (int i = 0; i < 3; i++)
            searchButtons[i].GetComponent<Image>().color = new Color(1, 1, 1, (i == cmd) ? 1 : 0);

        ISFSObject resObj = new SFSObject();
        resObj.PutInt("searchkey", cmd);
        sfsManager.sfs.Send(new ExtensionRequest("getLeaderboard", resObj, null));
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void populateLeaderboardList(SFSObject obj)
    {
        clearLeaderboardList();

        ISFSArray coachArray = obj.GetSFSArray("array");
        int rank = 1;
        foreach (ISFSObject item in coachArray)
        {
            GameObject newListItem = Instantiate(playerItem) as GameObject;
            PlayerItem newItem = newListItem.GetComponent<PlayerItem>();
            newItem.avatar.sprite = avatarController.avatar[item.GetInt("avatar")];
            newItem.nameText.text = item.GetUtfString("username");
            newItem.chipText.text = item.GetInt("chipcount").ToString();
            newItem.RankingText.text = rank.ToString();
            newItem.levelSlider.value = Mathf.Ceil((float)item.GetDouble("hour"));
            if (rank == 1)
                newItem.badge.SetActive(true);

            newListItem.transform.SetParent(listContent, false);
            rank++;
        }
    }

    public void clearLeaderboardList()
    {
        foreach (Transform child in listContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

}
