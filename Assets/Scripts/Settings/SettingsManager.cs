﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class SettingsManager : MonoBehaviour
{

    public GameObject panel;
    public Text nameText;

    private SFSManager sfsManager;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            Application.LoadLevel("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        panel.GetComponent<CanvasGroup>().interactable = !GlobalInfo.isCoach;

        init();
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    OnBackButtonPressed();
        //}
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnBackButtonPressed()
    {
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "loadLevel");
            obj.PutUtfString("param", "Main Screen");
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }
        //Application.LoadLevel("Main Screen");
        panel.SetActive(false);
    }

    public void onLogout()
    {
        CoachController cc = (CoachController)GameObject.FindObjectOfType(typeof(CoachController));
        if (GlobalInfo.isCoachMode)
            cc.sendAction(4);
        GlobalInfo.isCoachMode = false;
        GlobalInfo.isWaiting = false;
        GlobalInfo.isCoach = false;
        sfsManager.sfs.Disconnect();
        //sfsManager.sfs.Send(new LogoutRequest());
        //Application.LoadLevel("Login");
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void init()
    {
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "updateName");
            obj.PutUtfString("param", GlobalInfo.name);
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }
        updateName(GlobalInfo.name);
    }

    public void updateName(string _name)
    {
        nameText.text = _name;
    }

}
