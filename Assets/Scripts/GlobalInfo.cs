﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalInfo {

    public static string Host;
    public static int TcpPort;
    public static int WSPort;

    public static int loginState = 0;
    public static string zone;
    public static string sceneName;
    public static bool isGuest = false;
    public static bool isLogin = false;
    public static int id = -1;
    public static int avatar = 0;
    public static string email;
    public static string name;
    public static string password;
    public static int cash = 0;
    public static int clubchip = 0;
    public static int playMode = 0;
    public static bool isCoachMode = false;
    public static bool isWaiting = false;
    public static bool isCoach = false;
    public static string coachEmail;
    public static string coachName;
    public static int coachId;
    public static float coachTotalTime = 0f;
    public static float coachTimer = 0f;
    public static float coachTime = 300f;
    public static int buttonState = 0;
    public static int tableSize;
    public static bool isFast;
    public static bool isLobby = false;
    public static int blind;
    public static int buyin;
    public static string location;

}
