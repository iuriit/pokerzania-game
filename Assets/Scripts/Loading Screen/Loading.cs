﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class Loading : MonoBehaviour {

    //----------------------------------------------------------
    // Editor public properties
    //----------------------------------------------------------

    [Tooltip("IP address or domain name of the SmartFoxServer 2X instance")]
    public string Host = "127.0.0.1";

    [Tooltip("TCP port listened by the SmartFoxServer 2X instance; used for regular socket connection in all builds except WebGL")]
    public int TcpPort = 9933;

    [Tooltip("WebSocket port listened by the SmartFoxServer 2X instance; used for in WebGL build only")]
    public int WSPort = 8080;

    [Tooltip("Name of the SmartFoxServer 2X Zone to join")]
    public string Zone = "PokerGame";

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;

    // Use this for initialization
    void Start () {
        GlobalInfo.Host = Host;
        GlobalInfo.TcpPort = TcpPort;
        GlobalInfo.WSPort = WSPort;
        GlobalInfo.email = PlayerPrefs.GetString("email");
        GlobalInfo.password = PlayerPrefs.GetString("password");
        int login = PlayerPrefs.GetInt("login");
        GlobalInfo.isLogin = (login == 1) ? true: false;
        if (GlobalInfo.email == "" || !GlobalInfo.isLogin)
        {
            StartCoroutine(delay());
            return;
        }

        GlobalInfo.isGuest = false;
        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        Application.runInBackground = true;

        // Set connection parameters
        ConfigData cfg = new ConfigData();
        cfg.Host = GlobalInfo.Host;
#if !UNITY_WEBGL
        cfg.Port = GlobalInfo.TcpPort;
#else
		cfg.Port = GlobalInfo.WSPort;
#endif
        cfg.Zone = Zone;
        GlobalInfo.zone = Zone;

        // Initialize SFS2X client and add listeners
#if !UNITY_WEBGL
        sfsManager.sfs = new SmartFox();
        //sfs = new SmartFox();
#else
		sfsManager.sfs = new SmartFox(UseWebSocket.WS_BIN);
		//sfs = new SmartFox(UseWebSocket.WS_BIN);
#endif

        // Set ThreadSafeMode explicitly, or Windows Store builds will get a wrong default value (false)
        sfsManager.sfs.ThreadSafeMode = true;

        CoachController coachController = (CoachController)GameObject.FindObjectOfType(typeof(CoachController));

        sfsManager.reset();
        sfsManager.sfs.AddEventListener(SFSEvent.CONNECTION, sfsManager.OnConnection);
        sfsManager.sfs.AddEventListener(SFSEvent.CONNECTION_LOST, sfsManager.OnConnectionLost);
        sfsManager.sfs.AddEventListener(SFSEvent.LOGIN, sfsManager.OnLogin);
        sfsManager.sfs.AddEventListener(SFSEvent.LOGIN_ERROR, sfsManager.OnLoginError);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_JOIN, sfsManager.OnRoomJoin);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, sfsManager.OnRoomJoinError);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_ADD, sfsManager.OnRoomAdded);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_REMOVE, sfsManager.OnRoomRemoved);
        sfsManager.sfs.AddEventListener(SFSEvent.USER_ENTER_ROOM, sfsManager.OnUserEnterRoom);
        sfsManager.sfs.AddEventListener(SFSEvent.USER_EXIT_ROOM, sfsManager.OnUserExitRoom);
        sfsManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, sfsManager.OnExtensionResponse);
        sfsManager.sfs.AddEventListener(SFSEvent.PUBLIC_MESSAGE, sfsManager.OnPublicMessage);
        sfsManager.sfs.AddEventListener(SFSEvent.PRIVATE_MESSAGE, coachController.OnPrivateMessage);

        // Connect to SFS2X
        GlobalInfo.loginState = 1;
        sfsManager.sfs.Connect(cfg);
    }

    // Update is called once per frame
    void Update () {
		
	}

    IEnumerator delay()
    {
        yield return new WaitForSeconds(2f);
        Application.LoadLevel("Login");
    }
}
