﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class GameController : MonoBehaviour {

    public GameObject canvas;

    public GameObject chatPanel;
    public GameObject userInfoPanel;

    public Text chatText;
    public ScrollRect chatScrollView;
    public InputField chatInput;

    public GameObject exitButton;
    public GameObject playerSix;
    public GameObject playerNine;
    public GameObject[] players;
    public Sprite[] cardImages;
    public Text totalPot;
    public GameObject[] communityCards;
    public GameObject buttonGroup;
    public GameObject gage;
    public GameObject gageText;
    public Text backText;
    public Text descriptionText;
    public Text chipText;
    public Sprite[] actionImage;
    public Sprite[] blindImage;

    public Image avatarImage;
    public Text nameText;
    public Text chipcountText;
    public Slider levelSlider;
    public Text levelText;
    public Text handsPlayText;
    public Text biggestWinText;
    public Text hoursPlayedText;
    public Text vpipText;
    public Text pfrText;
    public Text afText;


    private static int NO_OF_RANKS = 13;
    private static int NO_OF_SUITS = 4;
    private static int CARD_BACK_NUM = 52;
    private static int CARD_PLACEHOLDER_NUM = 53;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;
    private Avatar avatarManager;
    private string betButtonText;
    private int actorPos;
    private bool actorShow;
    private bool isJoin = false;
    private float waitTime = 0f;
    private string[] blindText = new string[9];
    private string[] playerEmail = new string[9];
    private int orgPos;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            Application.LoadLevel("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        avatarManager = (Avatar)GameObject.FindObjectOfType(typeof(Avatar));
        canvas.GetComponent<CanvasGroup>().interactable = !GlobalInfo.isCoach;

        if (GlobalInfo.tableSize == 6)
        {
            playerSix.SetActive(true);
            for (int i = 0; i < 6; i++)
            {
                Transform ts = playerSix.transform.Find("Player" + (i + 1));
                players[i] = ts.gameObject;
            }
        }
        else
        {
            playerNine.SetActive(true);
            for (int i = 0; i < 9; i++)
            {
                Transform ts = playerNine.transform.Find("Player" + (i + 1));
                players[i] = ts.gameObject;
            }
        }

        if (GlobalInfo.playMode == 0)
            backText.text = GlobalInfo.location + " : " + (GlobalInfo.blind / 2) + "/" + GlobalInfo.blind;
        else if (GlobalInfo.playMode == 1)
            backText.text = "Tournament : " + (GlobalInfo.blind / 2) + "/" + GlobalInfo.blind;
        else if (GlobalInfo.playMode == 2)
            backText.text = "Club : " + (GlobalInfo.blind / 2) + "/" + GlobalInfo.blind;

        // Tell extension I'm ready to play
        sfsManager.sfs.Send(new ExtensionRequest("ready", new SFSObject(), sfsManager.sfs.LastJoinedRoom));
    }

    // Update is called once per frame
    void Update()
    {
        if(actorShow)
        {
            Transform ts;
            ts = players[actorPos].transform.Find("View/Select");
            ts.gameObject.GetComponent<Image>().fillAmount += Time.deltaTime / waitTime;
        }
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    OnExit();
        //}
    }

    // User Handler

    public void OnPreMessageButtonClick(string msg)
    {
        ISFSObject obj = new SFSObject();
        obj.PutUtfString("senderName", GlobalInfo.name);
        sfsManager.sfs.Send(new Sfs2X.Requests.PublicMessageRequest(msg, obj, sfsManager.sfs.LastJoinedRoom));
    }

    public void OnSendMessageButtonClick()
    {
        if (chatInput.text != "")
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("senderName", GlobalInfo.name);
            sfsManager.sfs.Send(new Sfs2X.Requests.PublicMessageRequest(chatInput.text, obj, sfsManager.sfs.LastJoinedRoom));

            // Reset message field
            chatInput.text = "";
        }
    }

    public void OnGageValueChanged()
    {
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "gageValChange");
            obj.PutInt("param", (int)gage.GetComponent<Slider>().value);
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }

        Transform ts;
        ts = buttonGroup.transform.Find("RaiseBetButton/Text");
        if (gage.GetComponent<Slider>().value == gage.GetComponent<Slider>().maxValue)
            ts.gameObject.GetComponent<Text>().text = "All-in";
        else
            ts.gameObject.GetComponent<Text>().text = betButtonText;
        gageText.GetComponent<Text>().text = string.Format("{0:n0}", gage.GetComponent<Slider>().value);
    }

    public void OnSit(int cmd)
    {
        if(GlobalInfo.isGuest)
        {
            canvas.GetComponent<CanvasGroup>().interactable = false;
            descriptionText.text = "You must not play as a guest.";
            StartCoroutine(DelayText());
            return;
        }

        orgPos = cmd;
        GameObject[] tmpObj = new GameObject[GlobalInfo.tableSize];
        for (int i = 0; i < GlobalInfo.tableSize; i ++)
            tmpObj[i] = players[i];
        for (int i = 0; i < GlobalInfo.tableSize; i++)
        {
            if (GlobalInfo.tableSize == 6)
                players[i] = tmpObj[(i - cmd + 4 + 6) % GlobalInfo.tableSize];
            else if (GlobalInfo.tableSize == 9)
                players[i] = tmpObj[(i - cmd + 5 + 9) % GlobalInfo.tableSize];
            Transform ts = players[i].transform.Find("View");
            ts.gameObject.SetActive(false);
        }

        ISFSObject resObj = new SFSObject();
        resObj.PutInt("pos", cmd);
        sfsManager.sfs.Send(new ExtensionRequest("join", resObj, sfsManager.sfs.LastJoinedRoom));
    }

    public void OnExit()
    {
        exitButton.SetActive(false);
        sfsManager.sfs.Send(new ExtensionRequest("exitRoom", new SFSObject(), sfsManager.sfs.LastJoinedRoom));
    }

    public void OnButtonClick(int cmd)
    {
        Transform ts;

        string str;
        int mode = 0;
        if (cmd == 1)
        {
            ts = buttonGroup.transform.Find("FoldButton/Text");
            str = ts.gameObject.GetComponent<Text>().text;
        }
        else if (cmd == 2)
        {
            ts = buttonGroup.transform.Find("CheckCallButton/Text");
            str = ts.gameObject.GetComponent<Text>().text;
        }
        else
            str = betButtonText;

        if (str == "Fold")
            mode = 1;
        else if(str == "Check")
            mode = 2;
        else if (str == "Call")
            mode = 3;
        else if (str == "Raise")
            mode = 4;
        else if (str == "Bet")
            mode = 5;

        ISFSObject resObj = new SFSObject();
        //resObj.PutUtfString("action", str);
        resObj.PutInt("action", mode);
        resObj.PutInt("value", (int)gage.GetComponent<Slider>().value);

        sfsManager.sfs.Send(new ExtensionRequest("action", resObj, sfsManager.sfs.LastJoinedRoom));
    }

    public void OnChatButton()
    {
        chatPanel.SetActive(true);
    }

    public void OnChatPanelCloseButton()
    {
        chatPanel.SetActive(false);
    }

    public void OnAvatarPressed(int cmd)
    {
        int pos = 0;
        if (GlobalInfo.tableSize == 6)
            pos = (cmd + orgPos - 4 + 6) % 6;
        else
            pos = (cmd + orgPos - 5 + 9) % 9;
        if (playerEmail[pos] == "")
            return;
        ISFSObject obj = new SFSObject();
        obj.PutUtfString("email", playerEmail[pos]);
        sfsManager.sfs.Send(new ExtensionRequest("getUserData", obj, null));
        sfsManager.sfs.Send(new ExtensionRequest("getUserInfo", obj, null));
        sfsManager.sfs.Send(new ExtensionRequest("cash", obj, null));
        userInfoPanel.SetActive(true);
    }

    public void OnUserInfoPanelBackButton()
    {
        userInfoPanel.SetActive(false);
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void printUserMessage(User user, string message, SFSObject obj)
    {
        chatText.text += "<b>" + (user == sfsManager.sfs.MySelf ? "You" : obj.GetUtfString("senderName")) + " :</b> " + message + "\n";

        // Scroll view to bottom
        chatScrollView.verticalNormalizedPosition = 0;
    }

    public void joinTable()
    {
        //chatPanel.SetActive(true);
        disableSitButtons();
    }

    public void disableSitButtons()
    {
        for (int i = 0; i < GlobalInfo.tableSize; i++)
        {
            Transform ts = players[i].transform.Find("SitButton");
            ts.gameObject.SetActive(false);
        }
    }

    public void resetPlayers()
    {
        for (int i = 0; i < GlobalInfo.tableSize; i++)
        {
            Transform ts = players[i].transform.Find("View");
            ts.gameObject.SetActive(false);
        }
    }

    public void kickPlayer(SFSObject obj)
    {
        string _email = obj.GetUtfString("email");
        if(_email == GlobalInfo.email)
        {
            sfsManager.sfs.Send(new Sfs2X.Requests.JoinRoomRequest("The Lobby"));
        }
        else
        {
            int _pos = obj.GetInt("pos");
            Transform ts;
            ts = players[_pos].transform.Find("View");
            ts.gameObject.SetActive(false);
        }
    }

    public void updatePlayer(SFSObject obj)
    {
        int _pos = obj.GetInt("pos");
        string _email = obj.GetUtfString("email");
        string _name = obj.GetUtfString("name");
        int _avatar = obj.GetInt("avatar");
        int _cash = obj.GetInt("cash");
        int _bet = obj.GetInt("bet");
        string _action = obj.GetUtfString("action");
        int _card1 = obj.GetInt("card1");
        int _card2 = obj.GetInt("card2");
        bool isNew = obj.GetBool("isNew");

        players[_pos].GetComponent<CanvasGroup>().alpha = (isNew || _action == "Fold" || _cash == 0) ? 0.5f : 1f;
        playerEmail[_pos] = _email;

        Transform ts;
        ts = players[_pos].transform.Find("View");
        ts.gameObject.SetActive(true);

        ts = players[_pos].transform.Find("SitButton");
        ts.gameObject.SetActive(false);

        ts = players[_pos].transform.Find("View/Photo");
        ts.gameObject.GetComponent<Image>().sprite = avatarManager.avatar[_avatar];
        //ts = players[_pos].transform.Find("View/Name");
        //ts.gameObject.GetComponent<Text>().text = _name;
        ts = players[_pos].transform.Find("View/Cash");
        ts.gameObject.GetComponent<Text>().text = "$" + _cash.ToString();
        //ts = players[_pos].transform.Find("View/Bet");
        //ts.gameObject.GetComponent<Text>().text = _bet.ToString();
        ts = players[_pos].transform.Find("View/Action");
        if (_action == "Call")
        {
            ts.gameObject.SetActive(true);
            ts.gameObject.GetComponent<Image>().sprite = actionImage[0];
        }
        else if (_action == "Check")
        {
            ts.gameObject.SetActive(true);
            ts.gameObject.GetComponent<Image>().sprite = actionImage[1];
        }
        else if (_action == "Raise" || _action == "Bet")
        {
            ts.gameObject.SetActive(true);
            ts.gameObject.GetComponent<Image>().sprite = actionImage[2];
        }
        else
            ts.gameObject.SetActive(false);

        ts = players[_pos].transform.Find("View/MyShowingCards");
        ts.gameObject.SetActive(_email == GlobalInfo.email);
        ts = players[_pos].transform.Find("View/ShowingCards");
        ts.gameObject.SetActive(_email != GlobalInfo.email);
        if (_email == GlobalInfo.email)
        {
            ts = players[_pos].transform.Find("View/MyShowingCards/Card01");
            ts.gameObject.SetActive(_card1 != CARD_PLACEHOLDER_NUM);
            ts.gameObject.GetComponent<Image>().sprite = cardImages[_card1];
            ts = players[_pos].transform.Find("View/MyShowingCards/Card02");
            ts.gameObject.SetActive(_card1 != CARD_PLACEHOLDER_NUM);
            ts.gameObject.GetComponent<Image>().sprite = cardImages[_card2];
        }
        else
        {
            ts = players[_pos].transform.Find("View/ShowingCards/Card01");
            ts.gameObject.SetActive(_card1 != CARD_PLACEHOLDER_NUM);
            ts.gameObject.GetComponent<Image>().sprite = cardImages[_card1];
            ts = players[_pos].transform.Find("View/ShowingCards/Card02");
            ts.gameObject.SetActive(_card1 != CARD_PLACEHOLDER_NUM);
            ts.gameObject.GetComponent<Image>().sprite = cardImages[_card2];
        }
    }

    public void changePlayerPos()
    {
    }

    public void updateBoard(SFSObject obj)
    {
        int[] cards = obj.GetIntArray("cards");
        int pot = obj.GetInt("pot");

        totalPot.text = "$" + pot.ToString();
        print(cards.Length);
        for(int i = 0; i < 5; i ++)
        {
            if (i < cards.Length)
            {
                communityCards[i].SetActive(true);
                communityCards[i].GetComponent<Image>().sprite = cardImages[cards[i]];
            }
            else
                communityCards[i].SetActive(false);
        }
    }

    public void getUserInput(SFSObject obj)
    {
        Transform ts;
        int minBet = obj.GetInt("minBet");
        int cash = obj.GetInt("cash");
        int actionSize = obj.GetInt("actionSize");

        buttonGroup.SetActive(true);
        //chatPanel.SetActive(false);

        for (int i = 0; i < actionSize; i ++)
        {
            string action = obj.GetUtfString("action" + i);
            switch(action)
            {
                case "Fold":
                    ts = buttonGroup.transform.Find("FoldButton");
                    ts.gameObject.SetActive(true);
                    ts = buttonGroup.transform.Find("FoldButton/Text");
                    ts.gameObject.GetComponent<Text>().text = action;
                    break;
                case "Call":
                case "Check":
                    ts = buttonGroup.transform.Find("CheckCallButton");
                    ts.gameObject.SetActive(true);
                    ts = buttonGroup.transform.Find("CheckCallButton/Text");
                    ts.gameObject.GetComponent<Text>().text = action;
                    break;
                case "Raise":
                case "Bet":
                    betButtonText = action;
                    ts = buttonGroup.transform.Find("RaiseBetButton");
                    ts.gameObject.SetActive(true);
                    ts = buttonGroup.transform.Find("RaiseBetButton/Text");
                    ts.gameObject.GetComponent<Text>().text = action;
                    gage.SetActive(true);
                    gage.GetComponent<Slider>().maxValue = cash;
                    if (minBet >= cash)
                    {
                        gage.GetComponent<Slider>().minValue = 0;
                        gage.GetComponent<Slider>().value = cash;
                        gage.GetComponent<Slider>().interactable = false;
                    }
                    else
                    {
                        gage.GetComponent<Slider>().minValue = minBet;
                        gage.GetComponent<Slider>().value = minBet;
                        gage.GetComponent<Slider>().interactable = true;
                    }
                    break;
            }
        }
    }

    public void setDealer(SFSObject obj)
    {
        int pos = obj.GetInt("dealerPos");
        bool isDealer = obj.GetBool("isDealer");
        Transform ts;
        ts = players[pos].transform.Find("View/Dealer");
        ts.gameObject.SetActive(isDealer);
        ts.gameObject.GetComponent<Image>().sprite = blindImage[0];
        blindText[pos] = isDealer ? "(D)" : "";
    }

    public void setBlind(SFSObject obj)
    {
        int pos = obj.GetInt("pos");
        string blText = obj.GetUtfString("blindText");

        Transform ts;
        for(int i = 0; i < 8; i ++)
        {
            if (blindText[i] == blText)
            {
                ts = players[i].transform.Find("View/Dealer");
                ts.gameObject.SetActive(false);
                blindText[i] = "";
            }
        }

        ts = players[pos].transform.Find("View/Dealer");
        if (blText == "(D)")
        {
            ts.gameObject.SetActive(true);
            ts.gameObject.GetComponent<Image>().sprite = blindImage[0];
        }
        else if (blText == "(SB)")
        {
            ts.gameObject.SetActive(true);
            ts.gameObject.GetComponent<Image>().sprite = blindImage[1];
        }
        else if (blText == "(BB)")
        {
            ts.gameObject.SetActive(true);
            ts.gameObject.GetComponent<Image>().sprite = blindImage[2];
        }
        else
            ts.gameObject.SetActive(false);
        blindText[pos] = blText;
    }

    public void setActor(SFSObject obj)
    {
        Transform ts;
        actorPos = obj.GetInt("actorPos");
        actorShow = obj.GetBool("isShow");
        int waitingTime = obj.GetInt("waitingTime");
        waitTime = waitingTime;

        ts = players[actorPos].transform.Find("View/Select");
        ts.gameObject.SetActive(actorShow);
        if(actorShow)
            ts.gameObject.GetComponent<Image>().fillAmount = 0;
    }

    public void hideButtons()
    {
        Transform ts;
        buttonGroup.SetActive(false);
        ts = buttonGroup.transform.Find("FoldButton");
        ts.gameObject.SetActive(false);
        ts = buttonGroup.transform.Find("CheckCallButton");
        ts.gameObject.SetActive(false);
        ts = buttonGroup.transform.Find("RaiseBetButton");
        ts.gameObject.SetActive(false);
        gage.SetActive(false);
        //chatPanel.SetActive(true);
    }

    public void setText(SFSObject obj)
    {
        String str = obj.GetUtfString("text");
        descriptionText.text = str;
        StartCoroutine("DelayText");
    }

    IEnumerator DelayText()
    {
        yield return new WaitForSeconds(3f);
        descriptionText.text = "";
        if(GlobalInfo.isGuest)
            canvas.GetComponent<CanvasGroup>().interactable = true;
    }

    public void getUserData(SFSObject obj)
    {
        Avatar avt = (Avatar)GameObject.FindObjectOfType(typeof(Avatar));
        avatarImage.sprite = avt.avatar[obj.GetInt("avatar")];
        nameText.text = obj.GetUtfString("username");
        hoursPlayedText.text = string.Format("{0:0.#}", obj.GetDouble("hour"));
        levelSlider.value = Mathf.Ceil((float)obj.GetDouble("hour"));
        levelText.text = "level " + levelSlider.value;
    }

    public void getUserInfo(SFSObject obj)
    {
        handsPlayText.text = obj.GetInt("handsPlay").ToString();
        biggestWinText.text = obj.GetInt("biggestWin").ToString();
        vpipText.text = obj.GetInt("vpip").ToString() + "%";
        pfrText.text = obj.GetInt("pfr").ToString() + "%";
        afText.text = string.Format("{0:0.00}", obj.GetFloat("af"));
    }

    public void updateCash(SFSObject obj)
    {
        ISFSArray array = obj.GetSFSArray("cashArray");
        chipcountText.text = array.GetSFSObject(0).GetInt("chipcount").ToString();
    }

}
