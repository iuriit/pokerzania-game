﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HoldemHand;


public class GameManager : MonoBehaviour {

    const int None      = 0;
    const int Fold      = 1;
    const int Call      = 2;
    const int Raise     = 3;
    const int Check     = 4;
    const int Bet       = 5;
    const int Busted    = 6;

    public Sprite[] playerImages;

    public GameObject[] playerObjects;
    public Sprite[] cardImages;
    public GameObject[] communityCardObjects;
    public GameObject buttonGroup;
    public GameObject gage;
    public GameObject gageText;
    public GameObject totalText;
    public GameObject description;
    public GameObject descriptionText;

    private int hCount = 0;
    private int[] playerIds;
    private Hand[] playerHands;
    private int totalPot;

    private int[] communityCardNumbers;
    private int[,] privateCardNumbers;
    private int[] totalChips;
    private int[] pots;
    private int[] status;

    private ArrayList curPlayers;

    private int mainPlayer = 5;
    private int playerNum = 8;
    private int sb = 0;
    private int bb = 1;
    private int sbVal;
    private int bbVal;
    private int curSel = 2;
    private int maxBetId;
    private int maxBetVal;
    private int prevMaxBetVal;
    private int step;
    private bool[] passFlags;
    private bool isCheckBet = false;

    private bool isPlaying = false;
    private GameObject timerObject;

    public void OnExitButtonPressed()
    {
        Application.LoadLevel("Main Screen");
    }

    // Use this for initialization
    void Start () {
        mainPlayer = 5;
        playerNum = playerObjects.Length;
        sb = 0;  bb = 1;    sbVal = 5;
        curSel = 5;

        playerIds = new int[playerNum];
        playerHands = new Hand[playerNum];

        communityCardNumbers = new int[5];
        privateCardNumbers = new int[playerNum, 2];
        totalChips = new int[playerNum];
        pots = new int[playerNum];
        status = new int[playerNum];
        passFlags = new bool[playerNum];

        bool[] flag = new bool[playerImages.Length];
        Transform ts;
        GameObject obj;
       
        for(int i = 0; i < playerNum; i ++)
        {
            int r = Random.Range(0, 11);
            if (flag[r])
            {
                i--;
                continue;
            }
            flag[r] = true;
            ts = playerObjects[i].transform.Find("View/Photo");
            obj = ts.gameObject;
            obj.GetComponent<Image>().sprite = playerImages[r];
            ts = playerObjects[i].transform.Find("View/Name");
            obj = ts.gameObject;
            obj.GetComponent<Text>().text = playerImages[r].name;

            status[i] = 0;
            totalChips[i] = 1500;
            pots[i] = 0;
            SetPlayer(i, status[i], totalChips[i], pots[i]);
            //curPlayers[i] = i;
        }

        StartCoroutine("InitGame", 1f);
    }

    int NextPlayer(int id)
    {
        int i;
        for (i = (id + 1) % playerNum; (status[i] == Fold || status[i] == Busted) && i != id; i = (i + 1) % playerNum) ;
        return i;
    }

    int PlayingCounts()
    {
        int s = 0;
        for(int i = 0; i < playerNum; i ++)
        {
            if (status[i] == Fold || status[i] == Busted)
                continue;
            s++;
        }
        return s;
    }

    void BetPlayer(int id, int v)
    {
        if (totalChips[id] + pots[id] < v)
        {
            pots[id] = totalChips[id] + pots[id];
            totalChips[id] = 0;
        }
        else
        {
            int tmp = v - pots[id];
            pots[id] = v;
            totalChips[id] -= tmp;
        }
    }

    public void SetPlayer(int id, int _status, int _totalChips, int _pot, bool bColor = false)
    {
        if (status[id] == Busted)
        {
            playerObjects[id].SetActive(false);
            return;
        }

        Transform ts;
        GameObject obj;

        status[id] = _status;
        totalChips[id] = _totalChips;
        pots[id] = _pot;

        playerObjects[id].GetComponent<CanvasGroup>().alpha = (_status == Fold) ? 0.5f : 1f;
        //playerObjects[id].GetComponent<CanvasGroup>().interactable = (status[id] == 1);

        ts = playerObjects[id].transform.Find("View/Status");
        obj = ts.gameObject;
        passFlags[id] = bColor;
        if(bColor)
            obj.GetComponent<Text>().color = new Color(0f, 1f, 0f);
        else
            obj.GetComponent<Text>().color = new Color(1f, 1f, 1f);
        switch (_status)
        {
            case None:
                obj.GetComponent<Text>().text = "";
                break;
            case Fold:
                obj.GetComponent<Text>().text = "Fold";
                break;
            case Call:
                obj.GetComponent<Text>().text = "Call";
                break;
            case Raise:
                obj.GetComponent<Text>().text = "Raise";
                break;
            case Check:
                obj.GetComponent<Text>().text = "Check";
                break;
            case Bet:
                obj.GetComponent<Text>().text = "Bet";
                break;
            case Busted:
                obj.GetComponent<Text>().text = "Busted";
                break;
        }
        if (id == sb)
            obj.GetComponent<Text>().text += " (SB)";
        if (id == bb)
            obj.GetComponent<Text>().text += " (BB)";

        ts = playerObjects[id].transform.Find("View/TotalChips");
        obj = ts.gameObject;
        obj.GetComponent<Text>().text = string.Format("{0:n0}", _totalChips) + "K";

        ts = playerObjects[id].transform.Find("View/Pot");
        obj = ts.gameObject;
        if (_pot == 0)
            obj.GetComponent<Text>().text = "";
        else
            obj.GetComponent<Text>().text = string.Format("{0:n0}", _pot) + "K";
    }

    void SetTotal()
    {
        totalPot = 0;
        for (int i = 0; i < playerNum; i++)
            totalPot += pots[i];
        totalText.GetComponent<Text>().text = string.Format("{0:n0}", totalPot) + "K";
    }

    IEnumerator InitGame(float _t)
    {
        yield return new WaitForSeconds(_t);

        Transform ts;
        GameObject obj;

        for (int i = 0; i < playerNum; i++)
        {
            ts = playerObjects[i].transform.Find("View/BackCards");
            obj = ts.gameObject;
            obj.SetActive(i != mainPlayer);
            ts = playerObjects[i].transform.Find("View/ShowingCards");
            obj = ts.gameObject;
            obj.SetActive(i == mainPlayer);
            ts = playerObjects[i].transform.Find("View/ShowingCards/Card01/CardFrame");
            obj = ts.gameObject;
            obj.SetActive(false);
            ts = playerObjects[i].transform.Find("View/ShowingCards/Card02/CardFrame");
            obj = ts.gameObject;
            obj.SetActive(false);
            for (int j = 0; j < 2; j++)
            {
                if (i == mainPlayer)
                {
                    ts = playerObjects[i].transform.Find("View/ShowingCards/Card0" + (j + 1));
                    obj = ts.gameObject;
                    obj.SetActive(false);
                }
                else
                {
                    ts = playerObjects[i].transform.Find("View/BackCards/Card0" + (j + 1));
                    obj = ts.gameObject;
                    obj.SetActive(false);
                }
            }
        }
        for (int i = 0; i < 5; i++)
            communityCardObjects[i].SetActive(false);

        bool[] check = new bool[52];
        for(int j = 0; j < 2; j ++)
        {
            for (int i = 0; i < playerNum; i++)
            {
                if (status[i] == Busted)
                    continue;
                int rv = Random.Range(0, 52);
                if(check[rv])
                {
                    i--;
                    continue;
                }
                check[rv] = true;
                yield return new WaitForSeconds(0.2f);
                privateCardNumbers[i, j] = rv;

                ts = playerObjects[i].transform.Find("View/BackCards");
                obj = ts.gameObject;
                obj.SetActive(i != mainPlayer);

                if (i == mainPlayer)
                {
                    ts = playerObjects[i].transform.Find("View/ShowingCards/Card0" + (j + 1));
                    obj = ts.gameObject;
                    obj.SetActive(true);
                    obj.GetComponent<Image>().sprite = cardImages[rv];
                }
                else
                {
                    ts = playerObjects[i].transform.Find("View/BackCards/Card0" + (j + 1));
                    obj = ts.gameObject;
                    obj.SetActive(true);
                }
            }
        }
        for(int i = 4; i >= 0; i --)
        {
            int rv = Random.Range(0, 52);
            if (check[rv])
            {
                i++;
                continue;
            }
            check[rv] = true;
            yield return new WaitForSeconds(0.2f);
            communityCardNumbers[i] = rv;
            communityCardObjects[i].SetActive(true);
            communityCardObjects[i].GetComponent<Image>().sprite = cardImages[52];
        }

        yield return new WaitForSeconds(_t);

        isPlaying = true;
        step = 0;
        curSel = NextPlayer(bb);
            
        bbVal = sbVal * 2;
        maxBetId = curSel;
        maxBetVal = bbVal;  prevMaxBetVal = 0;
        BetPlayer(sb, sbVal);
        BetPlayer(bb, bbVal);

        for (int i = 0; i < playerNum; i++)
        {
            SetPlayer(i, None, totalChips[i], pots[i]);
        }
        SetTotal();

        SelectPlayer(curSel, true);
    }

    // Update is called once per frame
    void Update () {
        if(isPlaying)
        {
            float v = timerObject.GetComponent<Slider>().value;
            if (v > 0)
            {
                timerObject.GetComponent<Slider>().value = v - Time.deltaTime;
                if (timerObject.GetComponent<Slider>().value == 0f)              // Timeout
                {
                    if (curSel == mainPlayer)
                        ButtonClickEvent(Fold);                                    // Fold
                    else
                    {
                        if (totalChips[curSel] > 0)
                        {
                            float rv = Random.Range(0f, 1f);
                            if (rv < 0.2f)
                                ButtonClickEvent(Fold);                                // Fold
                            else if (rv < 0.9f)
                                ButtonClickEvent(isCheckBet ? Check : Call);                                // Call/Check
                            else
                            {
                                int tmp = maxBetVal + (maxBetVal - prevMaxBetVal);

                                if (tmp - pots[curSel] < totalChips[curSel])
                                {
                                    tmp += Random.Range(0, 100);
                                    if (tmp - pots[curSel] > totalChips[curSel])
                                        tmp = totalChips[curSel] + pots[curSel];
                                }
                                ButtonClickEvent(isCheckBet ? Bet : Raise, tmp);
                            }
                        }
                        else
                            ButtonClickEvent(isCheckBet ? Check : Call);
                    }
                }
            }
        }
    }

    public void SelectPlayer(int id, bool bSelect)
    {
        Transform ts = playerObjects[id].transform.Find("View/Select");
        GameObject obj = ts.gameObject;
        obj.SetActive(bSelect);

        isCheckBet = (pots[id] == maxBetVal);
        print(id + "," + pots[id] + "," + maxBetVal);

        SetPlayer(id, 0, totalChips[id], pots[id]);
        //buttonGroup.GetComponent<CanvasGroup>().interactable = (bSelect && id == mainPlayer);
        buttonGroup.SetActive(bSelect && id == mainPlayer);
        gage.SetActive(bSelect && id == mainPlayer);

        if (bSelect)
        {
            if(totalChips[curSel] == 0)
            {
                ButtonClickEvent(None);
                return;
            }

            timerObject = ts.Find("Timer").gameObject;
            if (id == mainPlayer)
            {
                ts = buttonGroup.transform.Find("CheckCallButton/Text");
                obj = ts.gameObject;
                obj.GetComponent<Text>().text = isCheckBet ? "Check" : "Call";
                ts = buttonGroup.transform.Find("RaiseBetButton/Text");
                obj = ts.gameObject;
                obj.GetComponent<Text>().text = isCheckBet ? "Bet" : "Raise";

                timerObject.GetComponent<Slider>().value = 20f;

                //print(maxBetVal + "," + totalChips[id] + "," + pots[id]);
                if (maxBetVal - pots[id] >= totalChips[id])
                {
                    gage.SetActive(false);

                    ts = buttonGroup.transform.Find("RaiseBetButton");
                    obj = ts.gameObject;
                    obj.SetActive(false);
                }
                else
                {
                    int tmp = maxBetVal + (maxBetVal - prevMaxBetVal);

                    gage.GetComponent<Slider>().maxValue = totalChips[id];
                    if (tmp - pots[id] > totalChips[id])
                    {
                        gage.GetComponent<Slider>().interactable = false;
                        gage.GetComponent<Slider>().value = totalChips[id];
                    }
                    else
                    {
                        gage.GetComponent<Slider>().interactable = true;
                        gage.GetComponent<Slider>().minValue = tmp - pots[id];
                        gage.GetComponent<Slider>().value = tmp - pots[id];
                    }

                    ts = buttonGroup.transform.Find("RaiseBetButton");
                    obj = ts.gameObject;
                    obj.SetActive(true);
                }
            }
            else
                timerObject.GetComponent<Slider>().value = 1f;
        }
    }

    public void OnGageValueChanged()
    {
        Transform ts;
        GameObject obj;
        ts = buttonGroup.transform.Find("RaiseBetButton/Text");
        obj = ts.gameObject;
        if (gage.GetComponent<Slider>().value == totalChips[curSel])
            obj.GetComponent<Text>().text = "All-in";
        else
            obj.GetComponent<Text>().text = isCheckBet ? "Bet" : "Raise";
        gageText.GetComponent<Text>().text = string.Format("{0:n0}", gage.GetComponent<Slider>().value) + "K";
    }

    public void OnButtonPressed(int cmd)
    {
        if (isCheckBet && cmd != 1)
            cmd += 2;
        ButtonClickEvent(cmd, (int)(gage.GetComponent<Slider>().value) + pots[curSel]);
    }

    public void ButtonClickEvent(int _status, int _value = 0)                      // 1:Fold   2:Call/Check    3:Raise/Bet
    {
        print(maxBetVal + ", " + maxBetId);
        switch (_status)
        {
            case Call:case Check:
                BetPlayer(curSel, maxBetVal);
                break;
            case Raise:case Bet:
                maxBetId = curSel;
                prevMaxBetVal = maxBetVal;
                maxBetVal = _value;
                BetPlayer(curSel, maxBetVal);
                for (int i = 0; i < playerNum; i++)
                {
                    if (status[i] != Fold && status[i] != Busted)
                        status[i] = 0;
                    SetPlayer(i, status[i], totalChips[i], pots[i]);                    // Reset Color;
                }
                break;
        }
        SelectPlayer(curSel, false);
        status[curSel] = _status;
        SetPlayer(curSel, _status, totalChips[curSel], pots[curSel], _status != Fold);
        SetTotal();
        isCheckBet = false;
        DetermineNextState();
    }

    public void DetermineNextState()
    {
        int tmp = curSel;
        curSel = NextPlayer(curSel);
        //print("BeforeSel : "+ tmp + "CurSel : " + curSel + " , MaxBetID : " + maxBetId + " , MaxBetVal : " + maxBetVal);
        if(passFlags[curSel])                              // No more raise/bet
        {
            step++;

            if (step == 1)
            {
                communityCardObjects[0].GetComponent<Image>().sprite = cardImages[communityCardNumbers[0]];
                communityCardObjects[1].GetComponent<Image>().sprite = cardImages[communityCardNumbers[1]];
                communityCardObjects[2].GetComponent<Image>().sprite = cardImages[communityCardNumbers[2]];
            }
            else if(step == 2)
                communityCardObjects[3].GetComponent<Image>().sprite = cardImages[communityCardNumbers[3]];
            else if(step == 3)
                communityCardObjects[4].GetComponent<Image>().sprite = cardImages[communityCardNumbers[4]];
            else                                           // is One Match Finished
            {
                print("One Match Finished");
                DetermineWinner();
                return;
            }

            if (PlayingCounts() == 1)                              // is One Match Finished
            {
                print("One Match PreFinished");
                DetermineWinner();
                return;
            }

            StartCoroutine("func", 2f);
        }
        else
            SelectPlayer(curSel, true);
    }

    IEnumerator func(float _t)
    {
        yield return new WaitForSeconds(_t);

        curSel = NextPlayer(sb - 1);
        maxBetId = curSel;
        prevMaxBetVal = maxBetVal - bbVal;
        for (int i = 0; i < playerNum; i++)
        {
            passFlags[i] = false;
            if (status[i] != Fold && status[i] != Busted)
                status[i] = 0;
            SetPlayer(i, status[i], totalChips[i], pots[i]);                    // Reset
        }
        SelectPlayer(curSel, true);
    }

    string GetHandString(int v)
    {
        string board = "";
        int c, n;
        c = v / 13;     n = v % 13;

        if (n == 0)
            board += "a";
        else if (n == 9)
            board += "t";
        else if (n == 10)
            board += "j";
        else if (n == 11)
            board += "q";
        else if (n == 12)
            board += "k";
        else
            board += n + 1;

        if (c == 0)
            board += "s";
        else if (c == 1)
            board += "d";
        else if (c == 2)
            board += "c";
        else if (c == 3)
            board += "h";
        else
            board = "back";

        return board;
    }


    void DetermineWinner()
    {
        string board;
        string pc;
        Transform ts;
        GameObject obj;

        board = GetHandString(communityCardNumbers[0]);
        for (int i = 1; i < 5; i++)
            board += " " + GetHandString(communityCardNumbers[i]);
        print("board : " + board);

        hCount = 0;
        
        for(int id = 0; id < playerNum; id ++)
        {
            if (status[id] == Fold || status[id] == Busted)
                continue;

            ts = playerObjects[id].transform.Find("View/BackCards");
            obj = ts.gameObject;
            obj.SetActive(false);
            ts = playerObjects[id].transform.Find("View/ShowingCards");
            obj = ts.gameObject;
            obj.SetActive(true);

            ts = playerObjects[id].transform.Find("View/ShowingCards/Card01");
            obj = ts.gameObject;
            obj.SetActive(true);
            obj.GetComponent<Image>().sprite = cardImages[privateCardNumbers[id, 0]];
            ts = playerObjects[id].transform.Find("View/ShowingCards/Card02");
            obj = ts.gameObject;
            obj.SetActive(true);
            obj.GetComponent<Image>().sprite = cardImages[privateCardNumbers[id, 1]];

            pc = GetHandString(privateCardNumbers[id, 0]) + " " + GetHandString(privateCardNumbers[id, 1]);

            print(" id:" + id + " status:" + status[id] + " pc:" + pc);
            playerIds[hCount] = id;
            playerHands[hCount++] = new Hand(pc, board);
            //playerHands.Add(new Hand(pc, board));
        }
        //  playerHands Sort
        for (int i = 0; i < hCount; i++)
        {
            for(int j = i + 1; j < hCount; j ++)
            {
                if(playerHands[i] < playerHands[j])
                {
                    Hand tmp = new Hand();
                    tmp = playerHands[i];
                    playerHands[i] = playerHands[j];
                    playerHands[j] = tmp;

                    int temp = playerIds[i];
                    playerIds[i] = playerIds[j];
                    playerIds[j] = temp;
                }
            }
        }
        for (int i = 0; i < hCount; i++)
        {
            print(playerHands[i].PocketCards);
        }

        buttonGroup.SetActive(false);
        isPlaying = false;
 
        StartCoroutine("MakeBudget", 2f);
    }

    IEnumerator MakeBudget(float _t)
    {
        yield return new WaitForSeconds(_t);

        int firstNum, allInVal = maxBetVal, foldVal = 0;
        bool bAllIn = false;
        for (firstNum = 1; firstNum < hCount && (playerHands[firstNum] == playerHands[0]); firstNum++) ;
        for (int i = 0; i < firstNum; i ++)
        {
            if(totalChips[playerIds[i]] == 0)
            {
                bAllIn = true;
                if(allInVal > pots[playerIds[i]])
                    allInVal = pots[playerIds[i]];
            }
        }

        for (int i = 0; i < playerNum; i++)
        {
            if (status[i] == Busted)
                continue;
            if (bAllIn)
            {
                if (status[i] == Fold)
                    foldVal += pots[i];
                else
                {
                    foldVal += (pots[i] < allInVal) ? pots[i] : allInVal;
                    totalChips[i] += (pots[i] > allInVal) ? pots[i] - allInVal : 0;
                }
            }
            pots[i] = 0;
        }

        print(playerHands[0].Description);
        descriptionText.GetComponent<Text>().text = playerHands[0].Description;
        description.SetActive(true);
        yield return new WaitForSeconds(3f);
        description.SetActive(false);
        yield return new WaitForSeconds(1f);

        for (int i = 0; i < firstNum; i ++)
        {
            if (bAllIn)
                totalChips[playerIds[i]] += foldVal / firstNum;
            else
                totalChips[playerIds[i]] += totalPot / firstNum;
        }

        totalPot = 0;
        SetTotal();

        for (int i = 0; i < playerNum; i ++)
        {
            if (totalChips[i] == 0)                     // Busted
                status[i] = Busted;
            else
                status[i] = None;
        }

        int tmp = sb;
        sb = NextPlayer(sb);
        bb = NextPlayer(sb);
        if (tmp > sb)
            sbVal += 5;

        for (int i = 0; i < playerNum; i++)
            SetPlayer(i, status[i], totalChips[i], pots[i]);

        int pc = PlayingCounts();
        if(pc == 1)                        // Finished
        {
            yield return new WaitForSeconds(1f);
            print("Congratulations!");
            print("You have won 1th place");
            descriptionText.GetComponent<Text>().text = "Congratulations!\n" + "You have won 1th place";
            description.SetActive(true);
        }
        else if (status[mainPlayer] == Busted)
        {
            yield return new WaitForSeconds(1f);
            print("Busted!");
            print("You have won " + (pc + 1) + "th place");
            descriptionText.GetComponent<Text>().text = "Busted!\n" + "You have won " + (pc + 1) + "th place";
            description.SetActive(true);
        }
        else
            StartCoroutine("InitGame", 5f);
    }

}


