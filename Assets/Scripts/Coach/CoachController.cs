﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class CoachController : MonoBehaviour {

    public GameObject timer;
    public GameObject extendPanel;
    public GameObject chatPanel;
    public ScrollRect chatScrollView;
    public Text chatText;
    public CanvasGroup chatControls;
    public GameObject waitingPanel;
    public Text waitingText;
    public GameObject closeButton;
    public GameObject acceptButton;
    public GameObject rejectButton;

    public GameObject reviewPanel;
    public InputField moreInput;
    public GameObject[] reviewObj;
    public Sprite[] starImage;

    private SFSManager sfsManager;
    private bool extendFlag = false;
    private float[] starVal = { 4, 4, 4, 4 };

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();

        //for (int i = 0; i < 4; i++)
        //{
        //    for (int j = 1; j <= 5; j++)
        //    {
        //        Transform ts = reviewObj[i].transform.Find("Star" + j);
        //        ts.gameObject.GetComponent<Button>().onClick.AddListener(() => OnStarPressed(i * 5 + (j - 1)));
        //    }
        //}
    }

    // Use this for initialization
    void Start () {
        chatPanel.SetActive(GlobalInfo.isCoachMode);
	}
	
	// Update is called once per frame
	void Update () {
        if(GlobalInfo.isCoachMode)
            GlobalInfo.coachTotalTime += Time.deltaTime;
        GlobalInfo.coachTimer = (GlobalInfo.coachTimer > 0) ? GlobalInfo.coachTimer - Time.deltaTime : 0;
        timer.SetActive(GlobalInfo.isCoachMode);
        chatPanel.SetActive(GlobalInfo.isCoachMode);
        waitingPanel.SetActive(GlobalInfo.isWaiting);
        extendPanel.SetActive(!extendFlag && GlobalInfo.isCoachMode && !GlobalInfo.isCoach && 30f < GlobalInfo.coachTimer && GlobalInfo.coachTimer < 60f);
        if (GlobalInfo.isCoachMode)
        {
            timer.GetComponent<Text>().text = string.Format("{0:00} : {1:00} : {2:00}", (int)GlobalInfo.coachTimer / 60 / 60, ((int)GlobalInfo.coachTimer / 60) % 60, (int)(GlobalInfo.coachTimer) % 60);
            if(GlobalInfo.coachTimer == 0f/* || sfsManager.sfs.UserManager.GetUserById(GlobalInfo.coachId) == null*/)
            {
                GlobalInfo.isCoachMode = false;
                GlobalInfo.isWaiting = false;
                if (GlobalInfo.isCoach)
                {
                    GlobalInfo.isCoach = false;
                    Application.LoadLevel("Main Screen");
                }
                else
                    reviewPanel.SetActive(true);
                sendAction(4);
            }
        }
        //else
        //    timer.GetComponent<Text>().text = "";
	}

    private void OnApplicationQuit()
    {
        if (GlobalInfo.isCoachMode)
            sendAction(4);
        if(sfsManager.sfs.IsConnected)
            sfsManager.sfs.Disconnect();
        //sfsManager.sfs.Send(new LogoutRequest());
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnSendMessageButtonClick()
    {
        InputField msgField = (InputField)chatControls.GetComponentInChildren<InputField>();
        if (msgField.text != "")
        {
            sfsManager.sfs.Send(new Sfs2X.Requests.PrivateMessageRequest(msgField.text, GlobalInfo.coachId));

            // Reset message field
            msgField.text = "";
            //msgField.ActivateInputField();
            //msgField.Select();
        }
    }

    public void onReject()
    {
        sendAction(2);
        waitingPanel.SetActive(false);
        chatPanel.SetActive(false);
        GlobalInfo.isWaiting = false;
        GlobalInfo.isCoachMode = false;
        GlobalInfo.isCoach = false;
    }

    public void onAccept()
    {
        sendAction(1);
        if(!GlobalInfo.isCoach)
        {
            extendFlag = false;
            GlobalInfo.isWaiting = false;
            GlobalInfo.isCoachMode = true;
            GlobalInfo.coachTotalTime = 0f;
            GlobalInfo.coachTimer = GlobalInfo.coachTime;
            waitingPanel.SetActive(false);
            chatPanel.SetActive(true);
            Application.LoadLevel("Main Screen");
        }
        else
        {
            waitingText.text = "Waiting for response...";
            closeButton.SetActive(true);
            acceptButton.SetActive(false);
            rejectButton.SetActive(false);
        }
    }

    public void onExtendAccept()
    {
        GlobalInfo.coachTimer += GlobalInfo.coachTime;
        sendAction(3);
        extendPanel.SetActive(false);
    }

    public void onExtendReject()
    {
        extendFlag = true;
        extendPanel.SetActive(false);
    }

    public void OnStarPressed(int n)
    {
        int type = n / 5;
        int value = n % 5 + 1;
        starVal[type] = value;
        for(int i = 1; i <= 5; i ++)
        {
            Transform ts = reviewObj[type].transform.Find("Star" + i);
            ts.gameObject.GetComponent<Image>().sprite = starImage[(i <= value) ? 0 : 1];
        }
    }

    public void OnReviewSubmit()
    {
        float rate = 0f;
        for (int i = 0; i < 4; i++)
            rate += starVal[i];
        rate *= 0.25f;

        ISFSObject obj = new SFSObject();
        obj.PutUtfString("email", GlobalInfo.email);
        obj.PutUtfString("coachEmail", GlobalInfo.coachEmail);
        obj.PutInt("id", GlobalInfo.coachId);
        obj.PutFloat("rate", rate);
        obj.PutFloat("hour", GlobalInfo.coachTotalTime / 3600f);
        obj.PutInt("gross", (int)(30 * (GlobalInfo.coachTotalTime / GlobalInfo.coachTime)));
        sfsManager.sfs.Send(new ExtensionRequest("rateCoach", obj, null));
        reviewPanel.SetActive(false);
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void sendAction(int _action)
    {
        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("sender", GlobalInfo.email);
        resObj.PutUtfString("senderName", GlobalInfo.name);
        resObj.PutUtfString("receiver", GlobalInfo.coachEmail);
        resObj.PutInt("action", _action);
        sfsManager.sfs.Send(new ExtensionRequest("hireCoach", resObj));
    }

    public void getAction(ISFSObject obj)
    {
        int _action = obj.GetInt("action");
        switch(_action)
        {
            case 4:
                GlobalInfo.isCoachMode = false;
                GlobalInfo.isWaiting = false;
                GlobalInfo.coachTimer = 0f;
                if (GlobalInfo.isCoach)
                {
                    GlobalInfo.isCoach = false;
                    Application.LoadLevel("Main Screen");
                }
                break;
            case -1:case 2:
                GlobalInfo.isCoach = false;
                GlobalInfo.isCoachMode = false;
                GlobalInfo.isWaiting = false;
                GlobalInfo.coachTimer = 0f;
                waitingText.text = (_action == -1) ? GlobalInfo.coachName + " is offline." : GlobalInfo.coachName + " has rejected.";
                closeButton.SetActive(false);
                acceptButton.SetActive(false);
                rejectButton.SetActive(false);
                StartCoroutine(delayShow());
                break;
            case 0:
                waitingText.text = obj.GetUtfString("senderName") + " wants to hire you.";
                GlobalInfo.isWaiting = true;
                GlobalInfo.isCoach = true;
                GlobalInfo.coachEmail = obj.GetUtfString("sender");
                GlobalInfo.coachName = obj.GetUtfString("senderName");
                User user = sfsManager.sfs.UserManager.GetUserByName(GlobalInfo.coachEmail);
                GlobalInfo.coachId = user.Id;
                waitingPanel.SetActive(true);
                closeButton.SetActive(false);
                acceptButton.SetActive(true);
                rejectButton.SetActive(true);
                break;
            case 1:
                if(GlobalInfo.isCoach)
                {
                    extendFlag = false;
                    GlobalInfo.isWaiting = false;
                    GlobalInfo.isCoachMode = true;
                    GlobalInfo.coachTotalTime = 0f;
                    GlobalInfo.coachTimer = GlobalInfo.coachTime;
                    waitingPanel.SetActive(false);
                    chatPanel.SetActive(true);
                    Application.LoadLevel("Main Screen");
                }
                else
                {
                    waitingText.text = GlobalInfo.coachName + " has accepted.";
                    closeButton.SetActive(false);
                    acceptButton.SetActive(true);
                    rejectButton.SetActive(true);
                }
                break;
            case 3:
                GlobalInfo.coachTimer += GlobalInfo.coachTime;
                break;
        }
    }

    IEnumerator delayShow()
    {
        yield return new WaitForSeconds(1f);
        GlobalInfo.isWaiting = false;
        waitingPanel.SetActive(false);
    }

    public void printSystemMessage(string message)
    {
        chatText.text += "<color=#808080ff>" + message + "</color>\n";

        // Scroll view to bottom
        chatScrollView.verticalNormalizedPosition = 0;
    }

    public void printUserMessage(User user, string message)
    {
        chatText.text += "<b>" + (user == sfsManager.sfs.MySelf ? "You" : GlobalInfo.coachName) + ":</b> " + message + "\n";
        // Scroll view to bottom
        chatScrollView.verticalNormalizedPosition = 0;
    }

    public void initReview()
    {
        for (int i = 0; i < 4; i++)
            OnStarPressed(i * 5 + 3);
    }

    //----------------------------------------------------------
    // SmartFoxServer event listeners
    //----------------------------------------------------------

    public void OnPrivateMessage(BaseEvent evt)
    {
        User sender = (User)evt.Params["sender"];
        string message = (string)evt.Params["message"];
        print(sender.Name);
        printUserMessage(sender, message);
    }

}
