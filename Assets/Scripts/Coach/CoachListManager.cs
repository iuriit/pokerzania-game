﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class CoachListManager : MonoBehaviour
{

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject canvas;
    public GameObject listPanel;
    public GameObject detailPanel;

    public Text chipText;
    public Image coachAvatar;
    public Text coachName;
    public InputField searchInput;
    public GameObject[] searchButtons;
    public GameObject online;
    public GameObject offline;
    public Transform coachListContent;
    public GameObject coachListItem;
    public GameObject searchPanel;
    public Dropdown language;
    public Dropdown specialization;
    public Dropdown stakes;
    public Dropdown levelOfPlay;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;
    private CoachController coachController;
    private Avatar avatarController;
    private SFSObject detailObj;
    private CoachItem curItem;
    private int searchField = 0;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            Application.LoadLevel("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        avatarController = (Avatar)GameObject.FindObjectOfType(typeof(Avatar));
        coachController = GameObject.FindObjectOfType<CoachController>();
        canvas.GetComponent<CanvasGroup>().interactable = !GlobalInfo.isCoach;

        chipText.text = GlobalInfo.cash.ToString();

        OnSelectSearchField(0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (listPanel.active)
                OnListBackButtonClick();
            else if (detailPanel.active)
                OnDetailBackButtonClick();
            else if(searchPanel.active)
            {
                listPanel.SetActive(true);
                searchPanel.SetActive(false);
            }
        }
        if(detailPanel.active)
        {
            if (sfsManager.sfs.UserManager.ContainsUserName(curItem.coachEmail))
            {
                online.SetActive(true);
                offline.SetActive(false);
            }
            else
            {
                online.SetActive(false);
                offline.SetActive(true);
            }
        }
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnSelectSearchField(int cmd)
    {
        searchField = cmd;
        for(int i = 0; i < 3; i ++)
            searchButtons[i].GetComponent<Image>().color = new Color(1, 1, 1, (i == cmd) ? 1 : 0);

        onSearchByName();
    }

    public void OnListBackButtonClick()
    {
        //if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        //{
        //    ISFSObject obj = new SFSObject();
        //    obj.PutUtfString("receiver", GlobalInfo.coachEmail);
        //    obj.PutUtfString("action", "coachListBack");
        //    sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        //}
        Application.LoadLevel("Main Screen");
    }

    public void OnDetailBackButtonClick()
    {
        listPanel.SetActive(true);
        detailPanel.SetActive(false);
    }

    public void OnCoachItemClick(ISFSObject obj, CoachItem _curItem)
    {
        detailObj = (SFSObject)obj;
        curItem = _curItem;
        listPanel.SetActive(false);
        detailPanel.SetActive(true);

        coachAvatar.sprite = curItem.avatar.sprite;
        coachName.text = curItem.nameLabel.text;
        //Transform ts;
        //ts = detailPanel.transform.Find("View/Name");
        //ts.gameObject.GetComponent<Text>().text = obj.GetUtfString("fullname");
        //ts = detailPanel.transform.Find("View/Language");
        //ts.gameObject.GetComponent<Text>().text = obj.GetUtfString("language");
        //ts = detailPanel.transform.Find("View/Specialization");
        //ts.gameObject.GetComponent<Text>().text = obj.GetUtfString("specialization");
        //ts = detailPanel.transform.Find("View/Stakes");
        //ts.gameObject.GetComponent<Text>().text = obj.GetUtfString("stakes");
        //ts = detailPanel.transform.Find("View/LevelOfPlay");
        //ts.gameObject.GetComponent<Text>().text = obj.GetUtfString("levelofplay");
        //ts = detailPanel.transform.Find("View/BioData");
        //ts.gameObject.GetComponent<InputField>().text = obj.GetUtfString("biodata");
    }

    public void onHireMeClick()
    {
        GlobalInfo.isWaiting = true;
        coachController.waitingPanel.SetActive(true);
        coachController.waitingText.text = "Waiting for response...";
        coachController.closeButton.SetActive(true);
        coachController.acceptButton.SetActive(false);
        coachController.rejectButton.SetActive(false);
        GlobalInfo.coachEmail = detailObj.GetUtfString("email");
        GlobalInfo.coachName = detailObj.GetUtfString("fullname");
        User user = sfsManager.sfs.UserManager.GetUserByName(GlobalInfo.coachEmail);
        GlobalInfo.coachId = user.Id;
        GlobalInfo.isCoach = false;
        coachController.sendAction(0);
    }

    public void onSearchByName()
    {
        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("name", searchInput.text);
        resObj.PutInt("searchkey", searchField);
        sfsManager.sfs.Send(new ExtensionRequest("getCoach", resObj, null));
    }

    public void onSearchPanel()
    {
        print("searchPanel");
        listPanel.SetActive(false);
        searchPanel.SetActive(true);
    }

    public void onSearchButton()
    {
        print("searchButton");
        ISFSObject obj = new SFSObject();
        obj.PutUtfString("language", language.options[language.value].text);
        obj.PutUtfString("specialization", specialization.options[specialization.value].text);
        obj.PutUtfString("stakes", stakes.options[stakes.value].text);
        obj.PutUtfString("levelofplay", levelOfPlay.options[levelOfPlay.value].text);
        sfsManager.sfs.Send(new ExtensionRequest("getCoach", obj, null));

        listPanel.SetActive(true);
        searchPanel.SetActive(false);
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void sendToCoach(int _status)
    {
        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("receiver", GlobalInfo.coachEmail);
        resObj.PutInt("status", 0);

        sfsManager.sfs.Send(new ExtensionRequest("hireCoach", resObj));
    }

    public void populateCoachList(SFSObject obj)
    {
        clearCoachList();
        print("populateCoachList");

        int rank = 1;
        ISFSArray coachArray = obj.GetSFSArray("coachArray");
        foreach (ISFSObject item in coachArray)
        {
            GameObject newListItem = Instantiate(coachListItem) as GameObject;
            CoachItem coachItem = newListItem.GetComponent<CoachItem>();
            coachItem.avatar.sprite = avatarController.avatar[item.GetInt("avatar")];
            coachItem.nameLabel.text = item.GetUtfString("fullname");
            coachItem.coachEmail = item.GetUtfString("email");
            coachItem.coachId = item.GetInt("id");
            coachItem.RankingText.text = rank.ToString();
            coachItem.rate = (float)item.GetDouble("rate");
            coachItem.hour = (float)item.GetDouble("hour");
            coachItem.gross = item.GetInt("gross");
            coachItem.levelText.text = coachItem.gross.ToString() + " earned";
            coachItem.hoursText.text = "Hours Coached : " + coachItem.hour.ToString() + " hr(s)";
            for(int i = 0; i < 5; i ++)
            {
                if (i < Mathf.FloorToInt(coachItem.rate))
                    coachItem.stars[i].fillAmount = 1;
                else if (i >= Mathf.CeilToInt(coachItem.rate))
                    coachItem.stars[i].fillAmount = 0;
                else
                    coachItem.stars[i].fillAmount = coachItem.rate - Mathf.Floor(coachItem.rate);
            }

            //if (sfsManager.sfs.UserManager.ContainsUserName(coachItem.coachEmail))
            //    coachItem.status.SetActive(true);

            if (item.GetUtfString("email") != GlobalInfo.email)
                coachItem.button.onClick.AddListener(() => OnCoachItemClick(item, coachItem));

            newListItem.transform.SetParent(coachListContent, false);
            rank++;
        }
    }

    public void clearCoachList()
    {
        foreach (Transform child in coachListContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

}
