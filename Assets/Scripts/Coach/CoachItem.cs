﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class CoachItem : MonoBehaviour 
{
    public bool isOnline;
    public Image avatar;
	public Button button;
    public Image[] stars;
	public Text nameLabel;
    public Text RankingText;
    public Text levelText;
    public Text hoursText;
    public Text rateText;
    public string coachEmail;
    public int coachId;
    public float rate;
    public float hour;
    public int gross;
}
