﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class CoachRegisterManager : MonoBehaviour
{

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject panel;
    public InputField nameInput;
    public Dropdown language;
    public Dropdown specialization;
    public Dropdown stakes;
    public Dropdown levelOfPlay;
    public InputField bioData;
    public Text errorText;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            Application.LoadLevel("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();

        // Enable interface
        enableUI(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackButtonClick();
        }
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnBackButtonClick()
    {
        //if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        //{
        //    ISFSObject obj = new SFSObject();
        //    obj.PutUtfString("receiver", GlobalInfo.coachEmail);
        //    obj.PutUtfString("action", "loadLevel");
        //    obj.PutUtfString("param", "Profile");
        //    sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        //}
        Application.LoadLevel("Profile");
    }

    public void OnRegister()
    {
        if(nameInput.text == "")
        {
            errorText.text = "Name must be valid.";
            nameInput.ActivateInputField();
            return;
        }
        if (bioData.text == "")
        {
            errorText.text = "Bio-Data must be valid.";
            bioData.ActivateInputField();
            return;
        }
        ISFSObject obj = new SFSObject();
        obj.PutInt("id", GlobalInfo.id);
        obj.PutUtfString("fullname", nameInput.text);
        obj.PutUtfString("language", language.options[language.value].text);
        obj.PutUtfString("specialization", specialization.options[specialization.value].text);
        obj.PutUtfString("stakes", stakes.options[stakes.value].text);
        obj.PutUtfString("levelofplay", levelOfPlay.options[levelOfPlay.value].text);
        obj.PutUtfString("biodata", bioData.text);
        sfsManager.sfs.Send(new ExtensionRequest("coachRegister", obj));
    }

    public void enableUI(bool enable)
    {
        panel.GetComponent<CanvasGroup>().interactable = enable && !GlobalInfo.isCoach;
        errorText.text = "";
    }

}
