﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class ProfileManager : MonoBehaviour
{

    public GameObject panel;
    public GameObject avatarPanel;

    public Image avatarImage;
    public Text nameText;
    public Text chipcountText;
    public Slider levelSlider;
    public Text levelText;

    public Text handsPlayText;
    public Text biggestWinText;
    public Text hoursPlayedText;
    public Text vpipText;
    public Text pfrText;
    public Text afText;

    private SFSManager sfsManager;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            Application.LoadLevel("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        panel.GetComponent<CanvasGroup>().interactable = !GlobalInfo.isCoach;

        init();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackButtonPressed();
        }
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnBackButtonPressed()
    {
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "loadLevel");
            obj.PutUtfString("param", "Main Screen");
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }
        Application.LoadLevel("Main Screen");
    }

    public void OnCoachRegister()
    {
        //if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        //{
        //    ISFSObject obj = new SFSObject();
        //    obj.PutUtfString("receiver", GlobalInfo.coachEmail);
        //    obj.PutUtfString("action", "loadLevel");
        //    obj.PutUtfString("param", "Coach Register");
        //    sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        //}
        Application.LoadLevel("Coach Register");
    }

    public void OnAvatarPressed()
    {
        panel.SetActive(false);
        avatarPanel.SetActive(true);
    }

    public void OnAvatarChoose(int cmd)
    {
        GlobalInfo.avatar = cmd;

        ISFSObject obj = new SFSObject();
        obj.PutInt("avatar", cmd);
        sfsManager.sfs.Send(new ExtensionRequest("setAvatar", obj, null));

        panel.SetActive(true);
        avatarPanel.SetActive(false);
        setAvatar(cmd);
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void init()
    {
        if (!GlobalInfo.isCoach && GlobalInfo.isCoachMode)
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("receiver", GlobalInfo.coachEmail);
            obj.PutUtfString("action", "updateName");
            obj.PutUtfString("param", GlobalInfo.name);
            obj.PutInt("avatar", GlobalInfo.avatar);
            sfsManager.sfs.Send(new ExtensionRequest("sendCoach", obj));
        }
        updateName(GlobalInfo.name, GlobalInfo.avatar);
        sfsManager.sfs.Send(new ExtensionRequest("cash", new SFSObject(), null));

        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("email", GlobalInfo.email);
        sfsManager.sfs.Send(new ExtensionRequest("getLevel", resObj, null));
        sfsManager.sfs.Send(new ExtensionRequest("getUserInfo", resObj, null));
    }

    public void setAvatar(int cmd)
    {
        Avatar avt = (Avatar)GameObject.FindObjectOfType(typeof(Avatar));
        avatarImage.sprite = avt.avatar[cmd];
    }

    public void updateName(string _name, int _avatar)
    {
        setAvatar(_avatar);
        nameText.text = _name;
    }

    public void updateCash(SFSObject obj)
    {
        ISFSArray cashArray = obj.GetSFSArray("cashArray");
        foreach(SFSObject item in cashArray)
        {
            chipcountText.text = string.Format("{0:n0}", item.GetInt("chipcount"));
        }
    }

    public void getLevel(SFSObject obj)
    {
        hoursPlayedText.text = string.Format("{0:0.#}", obj.GetDouble("hour"));
        levelSlider.value = Mathf.Ceil((float)obj.GetDouble("hour"));
        levelText.text = "level " + levelSlider.value;
    }

    public void getUserInfo(SFSObject obj)
    {
        handsPlayText.text = obj.GetInt("handsPlay").ToString();
        biggestWinText.text = obj.GetInt("biggestWin").ToString();
        vpipText.text = obj.GetInt("vpip").ToString() + "%";
        pfrText.text = obj.GetInt("pfr").ToString() + "%";
        afText.text = string.Format("{0:0.00}", obj.GetFloat("af"));
    }

}
