﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class ResetController : MonoBehaviour
{

    //----------------------------------------------------------
    // Editor public properties
    //----------------------------------------------------------

    [Tooltip("Name of the SmartFoxServer 2X Zone to join")]
    public string Zone = "PokerGame";

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject panel;
    public InputField emailInput;
    public Text errorText;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SmartFox sfs;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

#if UNITY_WEBPLAYER
		if (!Security.PrefetchSocketPolicy(Host, TcpPort, 500)) {
			Debug.LogError("Security Exception. Policy file loading failed!");
		}
#endif

        // Enable interface
        enableUI(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (sfs != null)
            sfs.ProcessEvents();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackButtonClick();
        }
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnBackButtonClick()
    {
        Application.LoadLevel("Login");
    }

    public void OnResetButtonClick()
    {
        enableUI(false);

        // Set connection parameters
        ConfigData cfg = new ConfigData();
        cfg.Host = GlobalInfo.Host;
#if !UNITY_WEBGL
        cfg.Port = GlobalInfo.TcpPort;
#else
		cfg.Port = GlobalInfo.WSPort;
#endif
        cfg.Zone = Zone;
        GlobalInfo.zone = Zone;

        // Initialize SFS2X client and add listeners
#if !UNITY_WEBGL
        sfs = new SmartFox();
#else
		sfs = new SmartFox(UseWebSocket.WS_BIN);
#endif

        // Set ThreadSafeMode explicitly, or Windows Store builds will get a wrong default value (false)
        sfs.ThreadSafeMode = true;

        sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
        sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
        sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);

        // Connect to SFS2X
        sfs.Connect(cfg);
    }

    public void onEditValChange()
    {
        errorText.text = "";
    }

    //----------------------------------------------------------
    // Private helper methods
    //----------------------------------------------------------

    private void enableUI(bool enable)
    {
        panel.GetComponent<CanvasGroup>().interactable = enable;
        errorText.text = "";
    }

    private void reset()
    {
        // Remove SFS2X listeners
        // This should be called when switching scenes, so events from the server do not trigger code in this scene
        sfs.RemoveAllEventListeners();

        // Enable interface
        enableUI(true);
    }

    //----------------------------------------------------------
    // SmartFoxServer event listeners
    //----------------------------------------------------------

    private void OnConnection(BaseEvent evt)
    {
        if ((bool)evt.Params["success"])
        {
            Debug.Log("SFS2X API version: " + sfs.Version);
            Debug.Log("Connection mode is: " + sfs.ConnectionMode);

            // Save reference to SmartFox instance; it will be used in the other scenes
            SmartFoxConnection.Connection = sfs;

            // Login

            sfs.Send(new Sfs2X.Requests.LoginRequest("", ""));
        }
        else
        {
            // Remove SFS2X listeners and re-enable interface
            reset();

            // Show error message
            errorText.text = "Connection Failed. Try again";
        }
    }

    private void OnConnectionLost(BaseEvent evt)
    {
        // Remove SFS2X listeners and re-enable interface
        reset();

        string reason = (string)evt.Params["reason"];

        if (reason != ClientDisconnectionReason.MANUAL)
        {
            // Show error message
            errorText.text = "Connection was lost; reason is: " + reason;
        }
    }

    private void OnLogin(BaseEvent evt)
    {
        ISFSObject objOut = new SFSObject();
        objOut.PutUtfString("email", emailInput.text);

        sfs.Send(new ExtensionRequest("resetPassword", objOut, null));
    }

    private void OnLoginError(BaseEvent evt)
    {
        // Disconnect
        sfs.Disconnect();

        // Remove SFS2X listeners and re-enable interface
        reset();

        //// Show error message
        //errorText.text = "Login failed: " + (string)evt.Params["errorMessage"];
    }

    private void OnExtensionResponse(BaseEvent evt)
    {
        string cmd = (string)evt.Params["cmd"];
        ISFSObject objIn = (SFSObject)evt.Params["params"];
        if(cmd == "resetPassword")
        {
            if(objIn.GetBool("success"))
            {
                if (sfs.IsConnected)
                    sfs.Disconnect();
                OnBackButtonClick();
            }
            else
            {
                enableUI(true);
                errorText.text = "The email does not exist.";
                emailInput.ActivateInputField();
            }
        }
    }
}
