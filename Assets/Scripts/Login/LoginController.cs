﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class LoginController : MonoBehaviour
{

    [Tooltip("Name of the SmartFoxServer 2X Zone to join")]
    public string Zone = "PokerGame";

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject selectPanel;
    public GameObject panel;
    public InputField emailInput;
    public InputField passwordInput;
    public Text errorText;
    public GameObject resetButton;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        Application.runInBackground = true;
        emailInput.text = PlayerPrefs.GetString("email");
        passwordInput.text = PlayerPrefs.GetString("password");

#if UNITY_WEBPLAYER
		if (!Security.PrefetchSocketPolicy(Host, TcpPort, 500)) {
			Debug.LogError("Security Exception. Policy file loading failed!");
		}
#endif

        if(GlobalInfo.loginState == 1)
        {
            selectPanel.SetActive(false);
            panel.SetActive(true);
        }
        else
        {
            selectPanel.SetActive(true);
            panel.SetActive(false);
        }
        // Enable interface
        enableLoginUI(true);
        GlobalInfo.isLogin = false;
        PlayerPrefs.SetInt("login", 0);
    }

    // Update is called once per frame
    void Update()
    {
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnLoginTypeSelectButton(int cmd)
    {
        GlobalInfo.loginState = cmd;
        if (GlobalInfo.loginState == 0)
            OnLoginButtonClick(true);
        else if(GlobalInfo.loginState == 1)
        {
            selectPanel.SetActive(false);
            panel.SetActive(true);
        }
    }

    public void OnCreateAccountButtonClick()
    {
        GlobalInfo.isLobby = false;
        Application.LoadLevel("CreateAccount");
    }

    public void OnLoginButtonClick(bool cmd)
    {
        // Set connection parameters
        ConfigData cfg = new ConfigData();
        cfg.Host = GlobalInfo.Host;
#if !UNITY_WEBGL
        cfg.Port = GlobalInfo.TcpPort;
#else
		cfg.Port = GlobalInfo.WSPort;
#endif
        cfg.Zone = Zone;
        GlobalInfo.zone = Zone;

        // Initialize SFS2X client and add listeners
#if !UNITY_WEBGL
        sfsManager.sfs = new SmartFox();
        //sfs = new SmartFox();
#else
		sfsManager.sfs = new SmartFox(UseWebSocket.WS_BIN);
		//sfs = new SmartFox(UseWebSocket.WS_BIN);
#endif

        // Set ThreadSafeMode explicitly, or Windows Store builds will get a wrong default value (false)
        sfsManager.sfs.ThreadSafeMode = true;

        CoachController coachController = (CoachController)GameObject.FindObjectOfType(typeof(CoachController));

        sfsManager.reset();
        enableLoginUI(false);
        sfsManager.sfs.AddEventListener(SFSEvent.CONNECTION, sfsManager.OnConnection);
        sfsManager.sfs.AddEventListener(SFSEvent.CONNECTION_LOST, sfsManager.OnConnectionLost);
        sfsManager.sfs.AddEventListener(SFSEvent.LOGIN, sfsManager.OnLogin);
        sfsManager.sfs.AddEventListener(SFSEvent.LOGIN_ERROR, sfsManager.OnLoginError);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_JOIN, sfsManager.OnRoomJoin);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, sfsManager.OnRoomJoinError);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_ADD, sfsManager.OnRoomAdded);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_REMOVE, sfsManager.OnRoomRemoved);
        sfsManager.sfs.AddEventListener(SFSEvent.USER_ENTER_ROOM, sfsManager.OnUserEnterRoom);
        sfsManager.sfs.AddEventListener(SFSEvent.USER_EXIT_ROOM, sfsManager.OnUserExitRoom);
        sfsManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, sfsManager.OnExtensionResponse);
        sfsManager.sfs.AddEventListener(SFSEvent.PUBLIC_MESSAGE, sfsManager.OnPublicMessage);
        sfsManager.sfs.AddEventListener(SFSEvent.PRIVATE_MESSAGE, coachController.OnPrivateMessage);

        GlobalInfo.isGuest = cmd;
        GlobalInfo.email = (cmd) ? "" : emailInput.text;
        GlobalInfo.password = (cmd) ? "" : passwordInput.text;
        print(GlobalInfo.password + "," + GlobalInfo.password.Length);
        if (GlobalInfo.password.Length == 0)
            GlobalInfo.password = "";

        // Connect to SFS2X
        sfsManager.sfs.Connect(cfg);
    }

    public void onEditValChange()
    {
        errorText.text = "";
        resetButton.SetActive(false);
    }

    public void onResetButton()
    {
        Application.LoadLevel("ResetPassword");
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void enableLoginUI(bool enable)
    {
        panel.GetComponent<CanvasGroup>().interactable = enable;
        errorText.text = "";
        resetButton.SetActive(false);
    }

}
