﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class SignupController : MonoBehaviour
{

    //----------------------------------------------------------
    // Editor public properties
    //----------------------------------------------------------

    [Tooltip("Name of the SmartFoxServer 2X Zone to join")]
    public string Zone = "PokerSignup";

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject panel;
    public InputField emailInput;
    public InputField nameInput;
    public InputField passwordInput;
    public InputField passwordConfirmInput;
    public Text errorText;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SmartFox sfs;
    private string CMD_Signup = "$SignUp.Submit";

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

#if UNITY_WEBPLAYER
		if (!Security.PrefetchSocketPolicy(Host, TcpPort, 500)) {
			Debug.LogError("Security Exception. Policy file loading failed!");
		}
#endif

        // Enable interface
        enableLoginUI(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (sfs != null)
            sfs.ProcessEvents();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackButtonClick();
        }
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnBackButtonClick()
    {
        if (sfs != null && sfs.IsConnected)
            sfs.Disconnect();
        if(GlobalInfo.isLobby)
            Application.LoadLevel("Main Screen");
        else
            Application.LoadLevel("Login");
    }

    public void OnCreateButtonClick()
    {
        if (passwordInput.text != passwordConfirmInput.text)
        {
            errorText.text = "The passwords you entered don't match. Please try again.";
            return;
        }

        enableLoginUI(false);

        // Set connection parameters
        ConfigData cfg = new ConfigData();
        cfg.Host = GlobalInfo.Host;
#if !UNITY_WEBGL
        cfg.Port = GlobalInfo.TcpPort;
#else
		cfg.Port = GlobalInfo.WSPort;
#endif
        cfg.Zone = Zone;
        GlobalInfo.zone = Zone;

        // Initialize SFS2X client and add listeners
#if !UNITY_WEBGL
        sfs = new SmartFox();
#else
		sfs = new SmartFox(UseWebSocket.WS_BIN);
#endif

        // Set ThreadSafeMode explicitly, or Windows Store builds will get a wrong default value (false)
        sfs.ThreadSafeMode = true;

        sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
        sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
        sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);

        // Connect to SFS2X
        sfs.Connect(cfg);
    }

    public void onEditValChange()
    {
        errorText.text = "";
    }

    //----------------------------------------------------------
    // Private helper methods
    //----------------------------------------------------------

    private void enableLoginUI(bool enable)
    {
        panel.GetComponent<CanvasGroup>().interactable = enable;
        errorText.text = "";
    }

    private void reset()
    {
        // Remove SFS2X listeners
        // This should be called when switching scenes, so events from the server do not trigger code in this scene
        sfs.RemoveAllEventListeners();

        // Enable interface
        enableLoginUI(true);
    }

    //----------------------------------------------------------
    // SmartFoxServer event listeners
    //----------------------------------------------------------

    private void OnConnection(BaseEvent evt)
    {
        if ((bool)evt.Params["success"])
        {
            Debug.Log("SFS2X API version: " + sfs.Version);
            Debug.Log("Connection mode is: " + sfs.ConnectionMode);

            // Save reference to SmartFox instance; it will be used in the other scenes
            SmartFoxConnection.Connection = sfs;

            // Login

            sfs.Send(new Sfs2X.Requests.LoginRequest("", ""));
        }
        else
        {
            // Remove SFS2X listeners and re-enable interface
            reset();

            // Show error message
            errorText.text = "Connection Failed. Try again";
        }
    }

    private void OnConnectionLost(BaseEvent evt)
    {
        // Remove SFS2X listeners and re-enable interface
        reset();

        string reason = (string)evt.Params["reason"];

        if (reason != ClientDisconnectionReason.MANUAL)
        {
            // Show error message
            errorText.text = "Connection was lost; reason is: " + reason;
        }
    }

    private void OnLogin(BaseEvent evt)
    {
        ISFSObject objOut = new SFSObject();
        objOut.PutUtfString("email", emailInput.text);
        objOut.PutUtfString("username", nameInput.text);
        objOut.PutUtfString("password", passwordInput.text);

        sfs.Send(new ExtensionRequest(CMD_Signup, objOut));
    }

    private void OnLoginError(BaseEvent evt)
    {
        // Disconnect
        sfs.Disconnect();

        // Remove SFS2X listeners and re-enable interface
        reset();

        // Show error message
        errorText.text = "Login failed: " + (string)evt.Params["errorMessage"];
    }

    private void OnExtensionResponse(BaseEvent evt)
    {
        string cmd = (string)evt.Params["cmd"];
        ISFSObject objIn = (SFSObject)evt.Params["params"];
        if(cmd == CMD_Signup)
        {
            if(objIn.ContainsKey("errorMessage"))
            {
                reset();
                string errorMsg = objIn.GetUtfString("errorMessage");
                errorMsg = errorMsg.ToLower();
                if (errorMsg.Contains("name"))
                    nameInput.ActivateInputField();
                else if (errorMsg.Contains("email"))
                    emailInput.ActivateInputField();
                else if (errorMsg.Contains("password"))
                    passwordInput.ActivateInputField();
                errorText.text = objIn.GetUtfString("errorMessage");
            }
            else if(objIn.ContainsKey("success"))
            {
                ISFSObject objOut = new SFSObject();
                objOut.PutUtfString("email", emailInput.text);
                sfs.Send(new ExtensionRequest("createCash", objOut, null));
            }
        }
        else if(cmd == "createCash")
        {
            PlayerPrefs.SetString("email", emailInput.text);
            PlayerPrefs.SetString("password", passwordInput.text);
            if (GlobalInfo.isLobby)
            {
                SFSManager sfsManager = (SFSManager)GameObject.FindObjectOfType(typeof(SFSManager));
                sfsManager.sfs.Disconnect();
            }
            else
                OnBackButtonClick();
            //Application.LoadLevel("Login");
        }
    }
}
