﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClubItem : MonoBehaviour {
    public Button button;
    public Text clubname;
    public Text id;
    public GameObject badge;
}
