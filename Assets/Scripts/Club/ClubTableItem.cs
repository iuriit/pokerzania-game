﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClubTableItem : MonoBehaviour {
    public Button button;
    public Text tableName;
    public Text blindText;
    public Text memberNumText;
    public Text startTimeText;
    public string roomName;
    public int blind;
    public int buyin;
    public int memberNum;
    public int tableSize;
}
