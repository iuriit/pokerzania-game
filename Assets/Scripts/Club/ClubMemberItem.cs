﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClubMemberItem : MonoBehaviour {
    public Button button;
    public Button sendOutButton;
    public Button claimBackButton;
    public Image avatar;
    public Text nameText;
    public Text chipText;
    public int club_chip;
    public string email;
}
