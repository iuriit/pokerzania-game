﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class ClubManager : MonoBehaviour
{

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject canvas;
    public GameObject listPanel;
    public GameObject createPanel;
    public GameObject searchPanel;
    public GameObject messagePanel;
    public GameObject detailPanel;
    public GameObject tradePanel;
    public GameObject tradeDetailPanel;
    public GameObject tradeDetailSliderPanel;
    public GameObject addTablePanel;

    public Transform clubListContent;
    public GameObject clubListItem;
    public InputField createPanelClubName;
    public InputField createPanelDescription;
    public Slider createPanelChipSlider;
    public Text createPanelChipText;
    public Text createPanelErrorText;
    public InputField searchPanelId;
    public InputField searchPanelClubName;
    public Text searchPanelErrorText;
    public Text detailPanelClubName;
    public Text detailPanelId;
    public InputField detailPanelDescription;
    public Text detailPanelClubChip;
    public GameObject detailPanelAddTableButton;
    public GameObject detailPanelTradeButton;
    public Transform detailPanelTableListContent;
    public GameObject detailPanelTableItem;
    public GameObject detailPanelTableView;
    public GameObject detailPanelParameterView;
    public Text detailPanelParameterChipText;
    public Text detailPanelParameterMemberText;
    public GameObject detailPanelLeftButton;
    public GameObject detailPanelRightButton;
    public Text tradePanelChipText;
    public InputField tradePanelSearchInput;
    public Transform tradePanelMemberListContent;
    public GameObject tradePanelMemberListItem;
    public InputField tradePanelAmount;
    public Text tradeDetailPanelChipText;
    public Image tradeDetailPanelAvatar;
    public Text tradeDetailPanelMemberNameText;
    public Text tradeDetailPanelMemberChipText;
    public Slider tradeDetailPanelSlider;
    public Text tradeDetailPanelSliderText;
    public InputField addTablePanelTableName;
    public GameObject addTablePanelSixButton;
    public GameObject addTablePanelNineButton;
    public Slider addTablePanelBlindSlider;
    public Slider addTablePanelBuyinSlider;
    public Slider addTablePanelDurationSlider;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;
    private Avatar avatarController;
    private SFSObject detailObj;
    private ClubMemberItem curMember;
    private bool sendStatus = false;
    private bool isSix = true;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        //if (!SmartFoxConnection.IsInitialized)
        //{
        //    Application.LoadLevel("Login");
        //    return;
        //}

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        canvas.GetComponent<CanvasGroup>().interactable = !GlobalInfo.isCoach;

        avatarController = GameObject.FindObjectOfType<Avatar>();

        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("email", GlobalInfo.email);
        sfsManager.sfs.Send(new ExtensionRequest("getClub", resObj, null));
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackButton();
        }
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void OnBackButton()
    {
        if(listPanel.active)
            Application.LoadLevel("Main Screen");
        else if(createPanel.active)
        {
            listPanel.SetActive(true);
            createPanel.SetActive(false);
            ISFSObject resObj = new SFSObject();
            resObj.PutUtfString("email", GlobalInfo.email);
            sfsManager.sfs.Send(new ExtensionRequest("getClub", resObj, null));
        }
        else if(searchPanel.active)
        {
            listPanel.SetActive(true);
            searchPanel.SetActive(false);
        }
        else if (detailPanel.active)
        {
            listPanel.SetActive(true);
            detailPanel.SetActive(false);
        }
        else if (tradePanel.active)
        {
            detailPanel.SetActive(true);
            tradePanel.SetActive(false);
            ISFSObject resObj = new SFSObject();
            resObj.PutInt("club_id", detailObj.GetInt("club_id"));
            sfsManager.sfs.Send(new ExtensionRequest("getClubTable", resObj, null));

            ISFSObject resObj1 = new SFSObject();
            resObj1.PutUtfString("email", GlobalInfo.email);
            resObj1.PutInt("club_id", detailObj.GetInt("club_id"));
            sfsManager.sfs.Send(new ExtensionRequest("getClubUser", resObj1, null));
        }
        else if (tradeDetailPanel.active)
        {
            tradePanel.SetActive(true);
            tradeDetailPanel.SetActive(false);

            ISFSObject resObj = new SFSObject();
            resObj.PutUtfString("email", GlobalInfo.email);
            resObj.PutInt("club_id", detailObj.GetInt("club_id"));
            sfsManager.sfs.Send(new ExtensionRequest("getClubUser", resObj, null));

            ISFSObject obj = new SFSObject();
            obj.PutUtfString("name", "");
            obj.PutInt("club_id", detailObj.GetInt("club_id"));
            sfsManager.sfs.Send(new ExtensionRequest("getClubMember", obj, null));
        }
        else if (tradeDetailSliderPanel.active)
        {
            tradeDetailPanel.SetActive(true);
            tradeDetailSliderPanel.SetActive(false);
        }
        else if(addTablePanel.active)
        {
            detailPanel.SetActive(true);
            addTablePanel.SetActive(false);

            ISFSObject resObj = new SFSObject();
            resObj.PutInt("club_id", detailObj.GetInt("club_id"));
            sfsManager.sfs.Send(new ExtensionRequest("getClubTable", resObj, null));
        }
    }

    public void OnClubItemClick(SFSObject obj)
    {
        detailObj = obj;
        print(detailObj.GetUtfString("owner_email"));
        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("email", GlobalInfo.email);
        resObj.PutInt("club_id", obj.GetInt("club_id"));
        sfsManager.sfs.Send(new ExtensionRequest("getClubUser", resObj, null));
        sfsManager.sfs.Send(new ExtensionRequest("getClubTable", resObj, null));

        ISFSObject resObj1 = new SFSObject();
        resObj1.PutUtfString("name", "");
        resObj1.PutInt("club_id", detailObj.GetInt("club_id"));
        sfsManager.sfs.Send(new ExtensionRequest("getClubMember", resObj1, null));
    }

    public void onAddButton()
    {
        listPanel.SetActive(false);
        createPanel.SetActive(true);
        sfsManager.sfs.Send(new ExtensionRequest("cash", new SFSObject(), null));
    }

    public void onCreateClubButton()
    {
        if(createPanelClubName.text == "")
        {
            createPanelErrorText.text = "Club name cannot be null.";
            return;
        }
        if (createPanelDescription.text == "")
        {
            createPanelErrorText.text = "Description cannot be null.";
            return;
        }
        createPanelErrorText.text = "";
        enableUI(false);
        ISFSObject obj = new SFSObject();
        obj.PutUtfString("email", GlobalInfo.email);
        obj.PutUtfString("club_name", createPanelClubName.text);
        obj.PutUtfString("description", createPanelDescription.text);
        obj.PutInt("club_chip", (int)(createPanelChipSlider.value));
        sfsManager.sfs.Send(new ExtensionRequest("createClub", obj));
    }

    public void onCreatePanelChipSliderValChange()
    {
        createPanelChipText.text = createPanelChipSlider.value.ToString();
    }

    public void onSearchPanel()
    {
        listPanel.SetActive(false);
        searchPanel.SetActive(true);
    }

    public void onSearchButton()
    {
        if(searchPanelId.text == "")
        {
            searchPanelErrorText.text = "Id cannot be null";
            return;
        }
        searchPanelErrorText.text = "";

        enableUI(false);
        ISFSObject obj = new SFSObject();
        obj.PutUtfString("email", GlobalInfo.email);
        obj.PutUtfString("club_id", searchPanelId.text);
        obj.PutUtfString("clubname", searchPanelClubName.text);
        sfsManager.sfs.Send(new ExtensionRequest("joinClub", obj, null));
    }

    public void onMessagePanelYesButton()
    {
        messagePanel.SetActive(false);

        ISFSObject obj = new SFSObject();
        obj.PutUtfString("email", GlobalInfo.email);
        obj.PutInt("club_id", detailObj.GetInt("club_id"));
        sfsManager.sfs.Send(new ExtensionRequest("joinClub", obj, null));
    }

    public void onMessagePanelNoButton()
    {
        messagePanel.SetActive(false);
    }

    public void onDetailPanelAddTableButton()
    {
        detailPanel.SetActive(false);
        addTablePanel.SetActive(true);
        addTablePanelSixButton.GetComponent<Image>().color = new Color(isSix ? 0.6f : 1f, isSix ? 0.6f : 1f, isSix ? 0.6f : 1f);
        addTablePanelNineButton.GetComponent<Image>().color = new Color(!isSix ? 0.6f : 1f, !isSix ? 0.6f : 1f, !isSix ? 0.6f : 1f);
    }

    public void onDetailPanelTradeButton()
    {
        detailPanel.SetActive(false);
        tradePanel.SetActive(true);

        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("email", GlobalInfo.email);
        resObj.PutInt("club_id", detailObj.GetInt("club_id"));
        sfsManager.sfs.Send(new ExtensionRequest("getClubUser", resObj, null));

        ISFSObject obj = new SFSObject();
        obj.PutUtfString("name", "");
        obj.PutInt("club_id", detailObj.GetInt("club_id"));
        sfsManager.sfs.Send(new ExtensionRequest("getClubMember", obj, null));
    }

    public void onDetailPanelTableItemClick(ClubTableItem item)
    {
        if (item.memberNum >= item.tableSize)
            return;
        if (item.buyin > GlobalInfo.clubchip)
            return;
        GlobalInfo.tableSize = item.tableSize;
        GlobalInfo.buyin = item.buyin;
        GlobalInfo.blind = item.blind;
        sfsManager.sfs.Send(new JoinRoomRequest(item.roomName));
    }

    public void onDetailPanelLeftButton()
    {
        print("a");
        detailPanelLeftButton.SetActive(false);
        detailPanelRightButton.SetActive(true);
        detailPanelParameterView.SetActive(false);
        detailPanelTableView.SetActive(true);
    }

    public void onDetailPanelRightButton()
    {
        detailPanelLeftButton.SetActive(true);
        detailPanelRightButton.SetActive(false);
        detailPanelParameterView.SetActive(true);
        detailPanelTableView.SetActive(false);
    }

    public void onTradePanelSearchInputEndEdit()
    {
        ISFSObject obj = new SFSObject();
        obj.PutUtfString("name", tradePanelSearchInput.text);
        sfsManager.sfs.Send(new ExtensionRequest("getClubMember", obj, null));
    }

    public void OnTradePanelClubMemberItemClick(ClubMemberItem item)
    {
        tradePanel.SetActive(false);
        tradeDetailPanel.SetActive(true);
        tradeDetailPanelChipText.text = GlobalInfo.clubchip.ToString();
        tradeDetailPanelAvatar.sprite = item.avatar.sprite;
        tradeDetailPanelMemberNameText.text = item.nameText.text;
        tradeDetailPanelMemberChipText.text = item.club_chip.ToString();
        curMember = item;
    }

    public void OnTradePanelSendOutButton(ClubMemberItem item)
    {
        ISFSObject obj = new SFSObject();
        obj.PutInt("club_id", detailObj.GetInt("club_id"));
        obj.PutUtfString("fromUser", GlobalInfo.email);
        obj.PutUtfString("toUser", item.email);
        int amount = int.Parse(tradePanelAmount.text);
        amount = (amount < 0) ? 0 : amount;
        amount = (amount > GlobalInfo.clubchip) ? GlobalInfo.clubchip : amount;
        obj.PutInt("amount", amount);
        sfsManager.sfs.Send(new ExtensionRequest("clubTrade", obj, null));
        GlobalInfo.clubchip -= amount;
        item.club_chip += amount;
        tradePanelChipText.text = GlobalInfo.clubchip.ToString();
        item.chipText.text = item.club_chip.ToString();
    }

    public void OnTradePanelClaimBackButton(ClubMemberItem item)
    {
        ISFSObject obj = new SFSObject();
        obj.PutInt("club_id", detailObj.GetInt("club_id"));
        obj.PutUtfString("toUser", GlobalInfo.email);
        obj.PutUtfString("fromUser", item.email);
        int amount = int.Parse(tradePanelAmount.text);
        amount = (amount < 0) ? 0 : amount;
        amount = (amount > item.club_chip) ? item.club_chip : amount;
        obj.PutInt("amount", amount);
        sfsManager.sfs.Send(new ExtensionRequest("clubTrade", obj, null));
        GlobalInfo.clubchip += amount;
        item.club_chip -= amount;
        tradePanelChipText.text = GlobalInfo.clubchip.ToString();
        item.chipText.text = item.club_chip.ToString();
    }

    public void onTradeDetailPanelSendOutButton()
    {
        sendStatus = false;
        tradeDetailPanelSlider.maxValue = GlobalInfo.clubchip;
        tradeDetailPanelSlider.value = 0;
        tradeDetailPanel.SetActive(false);
        tradeDetailSliderPanel.SetActive(true);
    }

    public void onTradeDetailPanelClaimBackButton()
    {
        sendStatus = true;
        tradeDetailPanelSlider.maxValue = curMember.club_chip;
        tradeDetailPanelSlider.value = 0;
        tradeDetailPanel.SetActive(false);
        tradeDetailSliderPanel.SetActive(true);
    }

    public void onTradeDetailPanelSliderValChange()
    {
        tradeDetailPanelSliderText.text = tradeDetailPanelSlider.value.ToString();
    }

    public void onTradeDetailSliderYesButton()
    {
        ISFSObject obj = new SFSObject();
        obj.PutInt("club_id", detailObj.GetInt("club_id"));
        obj.PutUtfString("fromUser", sendStatus ? curMember.email : GlobalInfo.email);
        obj.PutUtfString("toUser", sendStatus ? GlobalInfo.email : curMember.email);
        obj.PutInt("amount", (int)(tradeDetailPanelSlider.value));
        sfsManager.sfs.Send(new ExtensionRequest("clubTrade", obj, null));
        if (sendStatus)          // claim back
        {
            GlobalInfo.clubchip += (int)(tradeDetailPanelSlider.value);
            curMember.club_chip -= (int)(tradeDetailPanelSlider.value);
        }
        else
        {
            GlobalInfo.clubchip -= (int)(tradeDetailPanelSlider.value);
            curMember.club_chip += (int)(tradeDetailPanelSlider.value);
        }
        tradeDetailPanelChipText.text = GlobalInfo.clubchip.ToString();
        tradeDetailPanelMemberChipText.text = curMember.club_chip.ToString();
        
        OnBackButton();
    }

    public void onAddTablePanelSixButton()
    {
        isSix = true;
        addTablePanelSixButton.GetComponent<Image>().color = new Color(isSix ? 0.6f : 1f, isSix ? 0.6f : 1f, isSix ? 0.6f : 1f);
        addTablePanelNineButton.GetComponent<Image>().color = new Color(!isSix ? 0.6f : 1f, !isSix ? 0.6f : 1f, !isSix ? 0.6f : 1f);
    }

    public void onAddTablePanelNineButton()
    {
        isSix = false;
        addTablePanelSixButton.GetComponent<Image>().color = new Color(isSix ? 0.6f : 1f, isSix ? 0.6f : 1f, isSix ? 0.6f : 1f);
        addTablePanelNineButton.GetComponent<Image>().color = new Color(!isSix ? 0.6f : 1f, !isSix ? 0.6f : 1f, !isSix ? 0.6f : 1f);
    }

    public void onAddTablePanelStartButton()
    {
        enableUI(false);
        ISFSObject obj = new SFSObject();
        obj.PutInt("club_id", detailObj.GetInt("club_id"));
        obj.PutUtfString("tableName", addTablePanelTableName.text);
        obj.PutInt("tableSize", isSix ? 6 : 9);
        obj.PutInt("blind", (int)(addTablePanelBlindSlider.value) * 2);
        obj.PutInt("buyin", (int)(addTablePanelBuyinSlider.value));
        obj.PutInt("duration", (int)(addTablePanelDurationSlider.value));
        sfsManager.sfs.Send(new ExtensionRequest("clubAddTable", obj, null));
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void enableUI(bool flag)
    {
        canvas.GetComponent<CanvasGroup>().interactable = flag;
    }

    public void joinClub(SFSObject obj)
    {
        enableUI(true);
        if(obj.GetBool("success"))
        {
            listPanel.SetActive(true);
            searchPanel.SetActive(false);
            ISFSObject resObj = new SFSObject();
            resObj.PutUtfString("email", GlobalInfo.email);
            sfsManager.sfs.Send(new ExtensionRequest("getClub", resObj, null));
        }
        else
            searchPanelErrorText.text = "Club does not exist";
    }

    public void populateClubList(SFSObject obj)
    {
        enableUI(true);
        clearClubList();

        ISFSArray array = obj.GetSFSArray("array");
        foreach (SFSObject item in array)
        {
            GameObject newListItem = Instantiate(clubListItem) as GameObject;
            ClubItem clubItem = newListItem.GetComponent<ClubItem>();
            clubItem.clubname.text = item.GetUtfString("club_name");
            clubItem.id.text = "ID: " + item.GetInt("club_id");

            print(item.GetUtfString("owner_email"));
            clubItem.button.onClick.AddListener(() => OnClubItemClick(item));

            newListItem.transform.SetParent(clubListContent, false);
        }
    }

    public void clearClubList()
    {
        foreach (Transform child in clubListContent)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void updateCash()
    {
        createPanelChipSlider.maxValue = GlobalInfo.cash;
        createPanelChipSlider.value = 0;
    }

    public void getClubUser(SFSObject obj)
    {
        bool isExist = obj.GetBool("isExist");
        if(isExist)
        {
            if(listPanel.active)
            {
                listPanel.SetActive(false);
                detailPanel.SetActive(true);
            }

            ISFSObject dataObj = obj.GetSFSObject("value");
            GlobalInfo.clubchip = dataObj.GetInt("club_chip");
            detailPanelClubName.text = detailObj.GetUtfString("club_name");
            detailPanelId.text = detailObj.GetInt("club_id").ToString();
            detailPanelClubChip.text = dataObj.GetInt("club_chip").ToString();
            detailPanelDescription.text = detailObj.GetUtfString("description");
            detailPanelParameterChipText.text = detailObj.GetInt("club_chip").ToString();
            print(detailObj.GetUtfString("owner_email") + " : " + dataObj.GetUtfString("email"));
            detailPanelAddTableButton.SetActive(detailObj.GetUtfString("owner_email") == dataObj.GetUtfString("email"));
            detailPanelTradeButton.SetActive(detailObj.GetUtfString("owner_email") == dataObj.GetUtfString("email"));
            tradePanelChipText.text = GlobalInfo.clubchip.ToString();
        }
        else
        {
            print("Not joined the club.");
        }
    }

    public void populateClubMemberList(SFSObject obj)
    {
        clearClubMemberList();

        ISFSArray array = obj.GetSFSArray("array");
        foreach (ISFSObject item in array)
        {
            if (item.GetUtfString("email") == GlobalInfo.email)
                continue;
            GameObject newListItem = Instantiate(tradePanelMemberListItem) as GameObject;
            ClubMemberItem memberItem = newListItem.GetComponent<ClubMemberItem>();
            memberItem.avatar.sprite = avatarController.avatar[item.GetInt("avatar")];
            memberItem.nameText.text = item.GetUtfString("username");
            memberItem.chipText.text = item.GetInt("club_chip").ToString();
            memberItem.email = item.GetUtfString("email");
            memberItem.club_chip = item.GetInt("club_chip");

            memberItem.sendOutButton.onClick.AddListener(() => OnTradePanelSendOutButton(memberItem));
            memberItem.claimBackButton.onClick.AddListener(() => OnTradePanelClaimBackButton(memberItem));

            newListItem.transform.SetParent(tradePanelMemberListContent, false);
        }
        detailPanelParameterMemberText.text = array.Size().ToString();
    }

    public void clearClubMemberList()
    {
        foreach (Transform child in tradePanelMemberListContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void populateClubTableList(SFSObject obj)
    {
        clearClubTableList();

        ISFSArray array = obj.GetSFSArray("array");
        foreach (ISFSObject item in array)
        {
            GameObject newListItem = Instantiate(detailPanelTableItem) as GameObject;
            ClubTableItem memberItem = newListItem.GetComponent<ClubTableItem>();
            memberItem.tableName.text = item.GetUtfString("table_name");
            memberItem.blindText.text = "Blinds : " + (item.GetInt("blind") / 2) + "/" + item.GetInt("blind");
            Room room = sfsManager.sfs.GetRoomByName(item.GetUtfString("room_name"));
            memberItem.memberNumText.text = room.UserCount.ToString() + "/" + item.GetInt("table_size");
            long st = (long)item.GetInt("start_time");
            print(st);
            TimeSpan ts = TimeSpan.FromMilliseconds(st * 1000);
            DateTime date = new DateTime() + ts;
            memberItem.startTimeText.text = date.ToString("HH:mm:ss");
            memberItem.roomName = item.GetUtfString("room_name");
            memberItem.buyin = item.GetInt("buyin");
            memberItem.blind = item.GetInt("blind");
            memberItem.memberNum = room.UserCount;
            memberItem.tableSize = item.GetInt("table_size");

            memberItem.button.onClick.AddListener(() => onDetailPanelTableItemClick(memberItem));

            newListItem.transform.SetParent(detailPanelTableListContent, false);
        }
    }

    public void clearClubTableList()
    {
        foreach (Transform child in detailPanelTableListContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

}
